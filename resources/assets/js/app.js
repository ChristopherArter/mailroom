
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('bootstrap-material-design');
// window.axios('vue-axios');
require('datatables.net');
require('datatables.net-dt');
require('datatables.net-responsive');
require('datatables.net-responsive-dt');
require('moment');
require('lodash');

import Swal from 'sweetalert2'
import _ from 'lodash';
window._ = _;
window.swal = Swal;
import dataTables from 'datatables.net';
window.dataTables = dataTables;
import Vue from 'vue';
window.Vue = Vue;
import axios from 'axios';
window.axios = axios;
import moment from 'moment';
window.moment = moment;
import Pikaday from 'pikaday';
window.Pikaday = Pikaday;

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI);

// set language to EN
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'

locale.use(lang)

// import DataTables and DataTablesServer together
import VueDataTables from 'vue-data-tables';
Vue.use(VueDataTables);

import endPointSetForm from './components/endPointSetForm.vue';
import endPointEdit from './components/endpoints/endPointEdit.vue';
import LeadTableRow from './components/leads/LeadTableRow.vue';
import LeadPreview from './components/leads/LeadPreview.vue';
import LeadsTable from './components/leads/LeadsTable.vue';
import SiteSelector from './components/sites/SiteSelector.vue';
import LeadFilter from './components/leads/LeadFilter.vue';
import FieldSelector from './components/endpoints/FieldSelector.vue';
import FieldMapForm from './components/endpoints/FieldMapForm.vue';
import FieldMapSet from './components/endpoints/FieldMapSet.vue';
import FieldMapTemplateSelector from './components/endpoints/FieldMapTemplateSelector.vue';
import EndPointSelector from './components/endpoints/EndPointSelector.vue';
import FieldMapTemplate from './components/endpoints/FieldMapTemplate.vue';

Vue.component('fieldmap-template', FieldMapTemplate);
Vue.component('endpoint-selector', EndPointSelector);
Vue.component('fieldmap-template-selector', FieldMapTemplateSelector);
Vue.component('fieldmap-set', FieldMapSet);
Vue.component('fieldmap-form', FieldMapForm);
Vue.component('field-selector', FieldSelector);
Vue.component('lead-filter', LeadFilter);
Vue.component('site-selector', SiteSelector);
Vue.component('lead-table-row', LeadTableRow);
Vue.component('end-point-set-form', endPointSetForm);
Vue.component('lead-preview', LeadPreview);
Vue.component('lead-table', LeadsTable);
Vue.component('endpoint-edit', endPointEdit);

// var slugify = function (text)
// {
//   return text.toString().toLowerCase()
//     .replace(/\s+/g, '-')           // Replace spaces with -
//     .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
//     .replace(/\-\-+/g, '-')         // Replace multiple - with single -
//     .replace(/^-+/, '')             // Trim - from start of text
//     .replace(/-+$/, '');            // Trim - from end of text
// }
// window.slugify = slugify();

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));

// const app = new Vue({
//     el: '#app'
// });
