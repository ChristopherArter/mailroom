export default  {

    removeUnderscores: function(string){
        return string.replace(/ /g,"_");
    },

    ucWords: function(string)
    {
        return string.replace(/\b\w/g, l => l.toUpperCase());
    },

    cleanUpString(string){
        string = this.removeUnderscores(string);
        string = this.ucWords(string);
        return string;
        
    },


    removeItemFromArray(item, array){

        var index = array.indexOf(item);
        if (index > -1) {
          array.splice(index, 1);
        }
      },

}
