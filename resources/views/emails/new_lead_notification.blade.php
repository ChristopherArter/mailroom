@extends('emails.email_master')



@section('email-content')

<tr>
        <td style="padding: 20px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">
            <h1 style="margin: 0 0 10px; font-size: 25px; line-height: 30px; color: #333333; font-weight: normal;">New Lead!</h1>
            <p>Great news, you've got a new lead! Here are the details below:</p>

            <?php
                
                $url = route('leads.show', $lead->id);
                $lead->created_at = new Carbon\Carbon($lead->created_at);
                $lead->created_at->subHours(4);
                $lead->lead_campaign = App\Campaign::find($lead->campaign_id);
                $slackChannel = $lead->getSite()->slack_channel;

            ?>
            <ul>
                <li>Date: <strong>{{ $lead->created_at->toFormattedDateString() }}</strong></li>
                <li>Time: <strong>{{ $lead->created_at->format('h:i A') }}</strong></li>
                <li>Campaign: <strong>{{ $lead->lead_campaign->name }}</strong></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td style="padding: 0 20px 20px;">
            <!-- Button : BEGIN -->
            <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" style="margin: auto;">
                <tr>
                    <td class="button-td button-td-primary" style="border-radius: 4px; background: #5C509F;">
                        <a class="button-a button-a-primary" href="{{ route('leads.show', $lead->id) }}" style="background: #5C509F; border: 0px solid #000000; font-family: sans-serif; font-size: 15px; line-height: 15px; text-decoration: none; padding: 13px 17px; color: #ffffff; display: block; border-radius: 4px;">View Lead</a>
                    </td>
                </tr>
            </table>
            <!-- Button : END -->
        </td>
    </tr>

    @endsection