@extends('emails.email_master')



@section('email-content')

<tr>
        <td style="padding: 20px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">
            <h1 style="margin: 0 0 10px; font-size: 25px; line-height: 30px; color: #333333; font-weight: normal;">New Lead!</h1>
            <?php
                
                $url = route('leads.show', $lead->id);
                $date = $lead->created_at = new Carbon\Carbon($lead->created_at);
                
                $lead->created_at->subHours(4);
                $leadCampaign = App\Campaign::find($lead->campaign_id);
                $slackChannel = $lead->getSite()->slack_channel;

            ?>
            <p><strong>Lead Information:</strong></p>
            <ul>
                <li>Date: <strong>{{ $date->toFormattedDateString() }}</strong></li>
                <li>Time: <strong>{{ $date->format('h:i A') }}</strong></li>
                <li>Campaign: <strong>{{ $leadCampaign->name }}</strong></li>
            </ul>

            <p><strong>Lead Fields:</strong></p>
            <ul>
                @foreach( $fields as $key => $value )

                    <li><?php echo ucwords(str_replace('_', ' ', $key)); ?>: <strong>{{ $value }}</strong></li>

                @endforeach
            </ul>
        </td>
    </tr>

    @endsection