@include('partials.head')
    <div id="mailroom">
    <div class="container-fluid">
        <div class="row no-gutters " style="min-height:1000px;">
            <div class="col-2 mr-sidebar">
@include('partials.sidebar')
            </div>
            <div class="col-10">
                    @include('partials.nav')
                    <div class="row">
                        <div class="col-12 mr-content">
                                @yield('content')
                        </div>
                    </div>
            </div>
        </div>
    </div>
    </div>
@include('partials.footer')
