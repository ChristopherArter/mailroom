@extends('layouts.master')


@section('sidebar')
{{-- LEAD PREVIEW --}}

    <lead-preview
    v-if="selectedLead"
    :lead="selectedLead"
    v-bind:loading="loading"
    v-on:remove-preview="selectedLead = null">
    </lead-preview>
    
@endsection


@section('top-nav-left')

{{-- SITE SELECT --}}
    <site-selector v-on:site-set="setSelectedSite"></site-selector>

@endsection

@section('content')

<table id="table2" class="table"></table>

<h2 v-if="selectedSite">Leads for @{{ selectedSite.name }}</h2>
{{-- LEAD TABLE --}}
        <lead-table
        
        v-if="selectedSite"
        
        v-on:querychange="queryChange"

        v-on:resend-leads="resendLeads"
        
        v-bind:total="total"
        
        v-on:preview-lead="previewLead"
        
        v-bind:leads = "leads">
    </lead-table>

@endsection



@section('footer-scripts')

<script>

    var mailroomVue = new Vue({
        el: '#mailroom',
        data: {
            leads: [],
            selectedLead: null,
            selectedSite: null,
            leadSelected: true,
            leadsSync: false,
            filter: {

                string: '',
                date_1: '',
                date_2: '',
            },
            loading: false,
            total:null,
        },

    created: function() {
        // this.setSelectedProperty(this.leads);
        // this.sites.forEach(function(site){
        // vm.set(site, 'selected', null);
        // });
    },

    mounted() {
        
    },

    computed: {
        totalSelected: function(){
            return this.selectedLeads.length
        },

        selectedLeads: function()
        {
            var selectedLeads = [];
            this.leads.forEach(function(lead)
            {
                if(lead.selected)
                {   
                    selectedLeads.push(lead);
                }
            });

            return selectedLeads;
        },
    },
        methods: {

            setSelectedSite($event)
            {
                this.selectedSite = $event;
                console.log('event fired');
                this.getLeads(this.selectedSite.id);
            },

            previewLead: function(lead){            
                this.selectedLead = lead;
            },

            getLeads: function(siteId = '1'){
            var vm = this;

            axios.get( baseUrl + '/json/leads/site/' + siteId, { headers:  { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }})
                .then( function(response){
                    vm.leads = response.data.data;
                    vm.total = response.data.total;
                    // console.log(vm.leads);
            }).catch(error => {
                if (error ) {
                    console.log(error);
                } 
                });
            },

            resendLeads: function(leads = null){
            
            var vm = this;
            if(!leads) {
               var sendLeads = vm.selectedLeads
            } else {
                var sendLeads = leads[0];
            }

             axios.post( baseUrl + '/json/leads/resend', sendLeads, { headers:  { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }})
            .then( function(response){
                console.log(response);
          }).catch(error => {
              if (error ) {
                  console.log(error);
              } 
            });
            },

            queryChange: function($event){
                
                var vm = this;
                vm.loading = true;
                // var data = [
                //     {
                //         event: $event,
                //         after_date: $event.filters[1][0],
                //         before_date: $event.filters[1][1]
                //     }
                // ];
                axios.post( baseUrl + '/json/leads/site/' + vm.selectedSite.id + '/query?page=' + $event.page, $event, { headers:  { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }})
                .then( function(response){

                vm.leads = response.data.data;
                vm.total = response.data.total;

                vm.loading = false;

                }).catch(error => {
                if (error ) {
                console.log(error);
                } 
                });
            }
        }
    
    });

</script>
<style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.bootstrap.css" integrity="sha256-bhW9Bw0HzXjIyeOwx/lYKdBHdTHLPIL2oPG0Gx/ad3g=" crossorigin="anonymous" />
    </style>


@endsection
