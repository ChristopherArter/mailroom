@extends('layouts.app')

@section('content')

<div id="intakes" class="container">
    <div v-for="intake in intakes" class="row">
        <div class="col-sm-12">
            
                    <p>Intake: <strong>@{{ intake.name }}</strong></p>
                    <p><pre>{{ url('/') }}/intakes/inbound/@{{intake.id}}</pre></p>
                
                    <ul class="list-unstyled">
                       
                            <li v-for="endPointSet in intake.end_point_sets" >
                                <end-point-set-form v-bind:endpointset="endPointSet"></end-point-set-form>
                            </li>
                            
                    </ul>                 


        </div>
    </div>
</div>

@endsection
@section('footer-scripts')
<script>
         
        var submarineVue = new Vue({
          el: '#intakes',
          data: {
            intakes: {!! $intakes !!},
          },
        
          methods:
          {
            addEndpoint: function(endPoints)
            {
                
               endPoints.push({
                   'url':''
               })
            },
          },
        });
        
        </script>
@endsection