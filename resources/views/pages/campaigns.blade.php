@extends('layouts.master')


@section('content')
<div class="row">
    <div style="margin-bottom:20px;" class="col-12">
        <button class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">New Route</button>
    </div>
</div>
      <!-- Modal -->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-12">
                            <label>Name</label>
                        <input v-model="newCampaign.name" type="text" class="form-control">
                    </div>

                    <div class="col-12">
                            <label>Key</label>
                            <input v-model="newCampaign.key" type="text" class="form-control">
                        </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" @click="createNewCampaign()" class="btn btn-primary">Save changes</button>
            </div>
          </div>
        </div>
      </div>


<div class="row">
    <div class="col-12">
        <div id="accordion">
            <div v-for="campaign in campaigns" class="card">
              <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" v-bind:data-target="'#collapse_' + campaign.id" aria-expanded="true" aria-controls="collapseOne">
                    @{{ campaign.name }}
                  </button>
                  <button class="btn btn-secondary float-right" @click="deleteCampaign(campaign)">Delete</button>
                </h5>
                
              </div>
          
              <div v-bind:id="'collapse_' + campaign.id" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                  
                    <ul v-if="campaign.end_points"  class="list-group">
                        <li v-for="endpoint in campaign.end_points" class="list-group-item">
                            @{{ endpoint.name  }}
                            <button @click="removeEndPoint(endpoint, campaign)" class="btn float-right btn-secondary btn-sm float-right">Remove</button>
                        </li>
                    </ul>

                    <button type="button" class="btn btn-primary" data-toggle="modal" v-bind:data-target="'#ep_modal_' + campaign.id">
                            Add Endpoint
                    </button>

                <!-- Modal -->
                <div class="modal fade" v-bind:id="'ep_modal_' + campaign.id" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel">Add End Point</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <div class="modal-body">

                            <div v-if="!campaign.endpoint_form">
                                    Select endpoint:
                                    <div class="row">
                                        <div class="col-8">
                                                <endpoint-selector v-on:endpoint-selected="stagedEndpoint = $event"></endpoint-selector>
                                        </div>
                                        <div class="col-4">
                                            <button @click="syncEndPointWithCampaign(campaign)" class="btn btn-primary">Add</button>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="text-center">
                                        <p>or</p>
                                            <button
                                            @click="campaign.endpoint_form = true"
                                            class="btn btn-primary">Add New Endpoint</button>
                                    </div>
                            </div>

                            <div v-if="campaign.endpoint_form">
                                <button @click="campaign.endpoint_form = false" class="btn btn-secondary btn-sm">Back</button>
                                <endpoint-edit v-if="campaign.endpoint_form" v-on:save-endpoint="syncEndPointsWithCampaign($event, campaign)" :endpoint="{ 
                                    name:'',
                                    headers:[],
                                    filtered_fields: [],
                                    url: '',
                                    
                                }"></endpoint-edit>
                            </div>
                        
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                    </div>
                </div>


                </div>
              </div>
            </div>
          </div>
    </div>
</div>
@endsection

@section('footer-scripts')

<script>

    var mailroomVue = new Vue({
        el: '#mailroom',
        data: {
            campaigns: [],
            stagedEndpoint : '',
            newCampaign: {
                name:'',
                key:'',
            }
        },

    created: function() {


    },

    mounted() {
        this.getCampaigns();
    },

    computed: {

    },
        methods: {

            getCampaigns(){

            var vm = this;
            axios.get( baseUrl + '/json/campaigns', { headers:  { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }})
                .then( function(response) {
                    vm.campaigns = response.data;
                   
                vm.campaigns.forEach(function(campaign){
                    vm.$set(campaign, 'endpoint_form', false);
                })
            }).catch(error => {
                if (error ) {
                    console.log(error);
                } 
                });
            },

            syncEndPointsWithCampaign(endpoint, campaign){
                if(!campaign.end_points) {
                    campaign.end_points = [];
                }
                campaign.end_points.push(endpoint);

                axios.post( baseUrl + '/json/campaigns/' + campaign.id + '/endpoints/sync', campaign.end_points, { headers:  { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }})
                .then( function(response) {
                    vm.campaign = response.data;
                   
                vm.campaigns.forEach(function(campaign){
                    vm.$set(campaign, 'endpoint_form', false);
                    vm.$set(campaign, 'staged_endpoint', null);
                })

            }).catch(error => {
                if (error ) {
                    console.log(error);
                } 
                });
            },

            syncEndPointWithCampaign(campaign){

                var vm = this;
                axios.post( baseUrl + '/json/campaigns/' + campaign.id + '/endpoint/sync', vm.stagedEndpoint, { headers:  { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }})
                .then( function(response) {
                    campaign.end_points = response.data.end_points;
                vm.campaigns.forEach(function(campaign){
                    vm.$set(campaign, 'endpoint_form', false);
                })

                swal({
                    type:'success',
                    titleText:'Endpoint Assigned!',
                    toast:true,
                    position:'top-end',
                    showConfirmButton: false,
                    timer: 3000
                    });

            }).catch(error => {
                if (error ) {
                    swal({
                    type:'error',
                    titleText:'Oops!',
                    toast:true,
                    position:'top-end',
                    showConfirmButton: false,
                    timer: 3000
                    });
                } 
                });
            },

            detachEndPointFromCampaign(endpoint, campaign){

            },
            removeEndPoint(endpoint, campaign){
                var vm = this;
                axios.post( baseUrl + '/json/campaigns/' + campaign.id + '/endpoint/delete', endpoint, { headers:  { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }})
                .then( function(response) {
                vm.getCampaigns();
                vm.campaigns.forEach(function(campaign){
                    vm.$set(campaign, 'endpoint_form', false);
                })

                swal({
                    type:'success',
                    titleText:'Endpoint removed!',
                    toast:true,
                    position:'top-end',
                    showConfirmButton: false,
                    timer: 3000
                    });

            }).catch(error => {
                if (error ) {
                    swal({
                    type:'error',
                    titleText:'Oops!',
                    toast:true,
                    position:'top-end',
                    showConfirmButton: false,
                    timer: 3000
                    });
                } 
                });
            },


            deleteCampaign(campaign){

                    
                    var vm = this;
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                    if (result.value) {
  
                        axios.post( baseUrl + '/json/campaigns/' + campaign.id + '/delete', { headers:  { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }})
                .then( function(response) {
                vm.getCampaigns();
                vm.campaigns.forEach(function(campaign){
                    vm.$set(campaign, 'endpoint_form', false);
                })

                swal({
                    type:'success',
                    titleText:'Route removed!',
                    toast:true,
                    position:'top-end',
                    showConfirmButton: false,
                    timer: 3000
                    });

            }).catch(error => {
                if (error ) {
                    swal({
                    type:'error',
                    titleText:'Oops!',
                    toast:true,
                    position:'top-end',
                    showConfirmButton: false,
                    timer: 3000
                    });
                } 
                });
                    }
                    })
            },

            createNewCampaign(){


                var vm = this;
                axios.post( baseUrl + '/json/campaigns/store', vm.newCampaign, { headers:  { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }})
                .then( function(response) {

                vm.getCampaigns();
                vm.campaigns.forEach(function(campaign){
                    vm.$set(campaign, 'endpoint_form', false);
                })

                swal({
                    type:'success',
                    titleText:'Campaign created!',
                    toast:true,
                    position:'top-end',
                    showConfirmButton: false,
                    timer: 3000
                    });

            }).catch(error => {
                if (error ) {
                    swal({
                    type:'error',
                    titleText:'Oops!',
                    toast:true,
                    position:'top-end',
                    showConfirmButton: false,
                    timer: 3000
                    });
                } 
                });

            }

        }
    
    });

</script>

@endsection

