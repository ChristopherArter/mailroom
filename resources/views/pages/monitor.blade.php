@extends('layouts.master')


@section('content')
<el-table
:data="leads"
stripe
style="width: 100%">
<el-table-column
  prop="created_at"
  label="Date"
  width="180">
</el-table-column>
<el-table-column
  prop="channel"
  label="Channel"
  width="180">
</el-table-column>
<el-table-column
  prop="status"
  label="Status">
</el-table-column>
</el-table>
@endsection

@section('footer-scripts')

<script>

    var mailroomVue = new Vue({
        el: '#mailroom',
        data: {
            leads = [],
            latestLead = '',
        },

    created: function() {

    },

    mounted() {
        this.getLeads();
        this.latestLead = this.leads[0];
        setInterval(function() {
this.getNewLead();
}, 5000);
    },

    computed: {

    },
        methods: {

            getLeads(){
                var vm = this;
            axios.get( baseUrl + '/json/leads/monitor', { headers:  { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }})
                .then( function(response) {
                    vm.leads = response.data;
            }).catch(error => {
                if (error ) {
                    console.log(error);
                } 
                });
            }

            getNewLead(){

            var vm = this;
            axios.get( baseUrl + '/json/leads/monitor/latest', { headers:  { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }})
                .then( function(response) {
                    if(vm.latestLead.id !== response.data.id){
                        vm.leads.push(response.data);
                        vm.latestLead = response.data;
                        vm.leads.splice(-1,1);
                    }
            }).catch(error => {
                if (error ) {
                    console.log(error);
                } 
                });


            }

        }
    
    });

</script>

@endsection

