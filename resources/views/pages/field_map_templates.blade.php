@extends('layouts.master')


@section('content')
<div class="accordion" id="accordionExample">
        <div v-for="fieldMapTemplate in fieldMapTemplates" class="card">
          <div class="card-header" id="headingOne">
            <h5 class="mb-0">
              <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                @{{ fieldMapTemplate.name }}
              </button>
            </h5>
          </div>
      
          <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
             <div class="col-12">
                    <fieldmap-template :fieldmaptemplate="fieldMapTemplate"></fieldmap-template>
             </div>
            </div>
          </div>
        </div>
      </div>
@endsection

@section('footer-scripts')

<script>

    var mailroomVue = new Vue({
        el: '#mailroom',
        data: {
            fieldMapTemplates: [],
        },

    created: function() {

    },

    mounted() {
        this.getFieldMapTemplates();
    },

    computed: {

    },
        methods: {

            getFieldMapTemplates(){
                var vm = this;
            axios.get( baseUrl + '/json/fieldmaptemplates', { headers:  { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }})
                .then( function(response) {
                    vm.fieldMapTemplates = response.data;
            }).catch(error => {
                if (error ) {
                    console.log(error);
                } 
                });
            }

        }
    
    });

</script>

@endsection

