@extends('layouts.master')


@section('content')
<div class="row">
    <div class="col-12">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#new_endpoint_modal">
            Add Endpoint
        </button>
              <!-- Modal -->
      <div class="modal fade" id="new_endpoint_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">New Endpoint</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <endpoint-edit v-on:save-endpoint="getEndpoints" :fieldlist="fieldlist" :endpoint="{ 
                    name:'',
                    headers:[],
                    filtered_fields: [],
                    url: '',
                    
                }"></endpoint-edit>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<div v-for="endpoint in endpoints">
<div class="card">
    <div class="card-body">
        <div class="row">
                <div class="col-8">
                        @{{ endpoint.name }}
                    </div>
            
                    <div class="col-4 text-right">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary" data-toggle="modal" v-bind:data-target="'#' + endpoint.id + '_endpoint_modal'">
                            Edit
                        </button>
                        <el-button class="btn btn-primary" @click="confirmDelete(endpoint)">Remove</el-button>

                    </div>

                    </div>
        </div>
    </div>

      <!-- Modal -->
      <div class="modal fade" v-bind:id="endpoint.id + '_endpoint_modal'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">@{{ endpoint.name }}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <endpoint-edit :fieldlist="fieldlist" :endpoint="endpoint"></endpoint-edit>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection



@section('footer-scripts')

<script>

    var mailroomVue = new Vue({
        el: '#mailroom',
        data: {
            endpoints: [],
            fieldlist: [],
        },

    created: function() {

    },

    mounted() {
        this.getEndpoints();
        this.getFieldList();
    },

    computed: {

    },
        methods: {

            removeItemFromArray(item, array){

                var index = array.indexOf(item);
                if (index > -1) {
                array.splice(index, 1);
                }
            },

            getEndpoints(){
            var vm = this;
            axios.get( baseUrl + '/json/endpoints', { headers:  { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }})
                .then( function(response){
                    vm.endpoints = response.data;
            }).catch(error => {
                if (error ) {
                    console.log(error);
                } 
                });
            },

            deleteEndpoint(endpoint){

                var vm = this;
                axios.post( baseUrl + '/json/endpoints/delete/' + endpoint.id, { headers:  { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }})
                .then( function(response){
                    vm.getEndpoints();
                    swal({
              type:'success',
              titleText:'Deleted!',
              toast:true,
              position:'top-end',
              showConfirmButton: false,
              timer: 3000
            });
            }).catch(error => {
                if (error ) {
                    console.log(error);
                } 
                });

            },

            confirmDelete(endpoint){
                this.deleteEndpoint(endpoint);
        //         var vm = this;
        //         this.$confirm('This will permanently delete the file. Continue?', 'Warning', {
        //   confirmButtonText: 'OK',
        //   cancelButtonText: 'Cancel',
        //   type: 'warning'
        // }).then(() => {
        //     vm.deleteEndpoint(endpoint);
        //     swal({
        //       type:'success',
        //       titleText:'Deleted!',
        //       toast:true,
        //       position:'top-end',
        //       showConfirmButton: false,
        //       timer: 3000
        //     });
        // }).catch(() => {

        // });
            },

            getFieldList(){

                var vm = this;
                axios.get( baseUrl + '/settings/fields', { headers:  { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }})
                    .then( function(response){
                        vm.fieldlist = response.data;
                        }).catch(error => {
                            if (error ) {
                                vm.loading = false;
                                console.log(error);
                            } 
                    });
            }
        }
    
    });

</script>
<style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.bootstrap.css" integrity="sha256-bhW9Bw0HzXjIyeOwx/lYKdBHdTHLPIL2oPG0Gx/ad3g=" crossorigin="anonymous" />
    </style>


@endsection
