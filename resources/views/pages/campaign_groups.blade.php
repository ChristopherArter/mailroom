@extends('layouts.master')


@section('content')
<div class="row">
    <div class="col-12">
        <button class="btn btn-primary float-right"@click="createCampaignSet()">Add New</button>
    </div>
</div>
<div class="row">
    <div v-for="campaignset in campaignsets" class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6"><i class="far fa-folder"></i> @{{ campaignset.name }}</div>
                    <div class="col-2">
                            <small class="float-right" v-if="campaignset.campaigns.length > 0">@{{ campaignset.campaigns.length }} routes</small>
                    </div>
                    <div class="col-4">
                        <div class="btn-group float-right">
                                <button class="btn btn-secondary btn-small" @click="editCampaignSet(campaignset)" href="#">Edit</button>
                                <button class="btn btn-secondary btn-small" @click="deleteCampaignSet(campaignset)" >Delete</button>
                        </div>
                          </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <ul>
                                <li v-for="campaign in campaignset.campaigns">
                                    @{{ campaign.name }}
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection



@section('footer-scripts')

<script>

    var mailroomVue = new Vue({
        el: '#mailroom',
        data: {
            campaignsets: [],
            campaigns: [],
        },

    created: function() {

    },

    mounted() {
        this.getCampaignSets();
        this.getCampaigns();
    },

    computed: {

    },
        methods: {

            getCampaignSets(){

                var vm = this;
            axios.get( baseUrl + '/json/campaign-groups', { headers:  { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }})
                .then( function(response){
                    vm.campaignsets = response.data;
            }).catch(error => {
                if (error ) {
                    console.log(error);
                } 
                });
            },

            getCampaigns(){
                var vm = this;
                axios.get( baseUrl + '/json/campaign-groups', { headers:  { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }})
                    .then( function(response){
                        vm.campaignsets = response.data;
                }).catch(error => {
                    if (error ) {
                        console.log(error);
                    } 
                    });
            },

            saveCampaignSet(campaignset){
                var vm = this;
            axios.post( baseUrl + '/json/campaign-groups/sync', campaignset, { headers:  { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }})
                .then( function(response){
                    campaignset = response.data;
            }).catch(error => {
                if (error ) {
                    console.log(error);
                } 
                });
            },

            editCampaignSet(campaignset){
                var vm = this;
                swal({
                    title: 'Edit Route Group',
                    input: 'text',
                    inputValue: campaignset.name
                }).then((result) => {
                
                    if(result.value){
                        campaignset.name = result.value;
                    campaignset = vm.saveCampaignSet(campaignset);
                    } else {

                    }
                });
            },

            deleteCampaignSet(campaignset){
                var vm = this;
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                    if (result.value) {
  
                    axios.post( baseUrl + '/json/campaign-groups/delete', campaignset, { headers:  { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }})
                        .then( function(response) {
                            vm.campaignsets = vm.getCampaignSets();
                    }).catch(error => {
                        if (error ) {
                            console.log(error);
                        } 
                        });
                    }
                    })
            },

            createCampaignSet(){
                var vm = this;
                swal({
                    title: 'Create Route Group',
                    input: 'text',
                }).then((result) => {
                
                    if(result.value){
                        var newCampaignSet = {
                            name: result.value
                        }
                    vm.saveCampaignSet(newCampaignSet);
                    vm.getCampaignSets();
                    } else {

                    }
                });
            }
        }
    
    });

</script>
<style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.bootstrap.css" integrity="sha256-bhW9Bw0HzXjIyeOwx/lYKdBHdTHLPIL2oPG0Gx/ad3g=" crossorigin="anonymous" />
    </style>


@endsection

