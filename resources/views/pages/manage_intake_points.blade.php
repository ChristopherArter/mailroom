@extends('layouts.app')

@section('content')
<div class="container">

   <div class="row">
       <div class="col-sm-12">
            @foreach($intakePoints as $Site)
                <div class="card">
                    <div class="card-header">
                        <strong> {{ $Site->name }}</strong>
                    </div>
                    <div class="card-body">
                        @foreach($Site->endPoints as $endPoint)
                        <p>{{ $endPoint->url }}</p>
                        
                        @endforeach
                    </div>
                </div>
            @endforeach
       </div>
   </div>

</div>
@endsection