<div class="sticky-top">
<a class="navbar-brand" href="{{ url('/') }}">
    <img width="150" src="{!! url('/') !!}/images/mailroom-logo.png">
</a>
{{-- <ul class="list-unstyled">
    <li>Lorem ipsum dolor sit amet</li>
    <li>Consectetur adipiscing elit</li>
    <li>Integer molestie lorem at massa</li>
    <li>Facilisis in pretium nisl aliquet</li>
    <li>Nulla volutpat aliquam velit</li>
</ul> --}}

@yield('sidebar')
</div>