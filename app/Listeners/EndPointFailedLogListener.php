<?php

namespace App\Listeners;

use App\Events\EndPointFailed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EndPointFailedLogListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EndPointFailed  $event
     * @return void
     */
    public function handle(EndPointFailed $event)
    {
        //
    }
}
