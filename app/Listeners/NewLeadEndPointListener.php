<?php

namespace App\Listeners;

use App\Events\NewLead;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewLeadEndPointListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewLead  $event
     * @return void
     */
    public function handle(NewLead $event)
    {
        $lead = $event->lead;
        //$lead->sendToAllEndPoints();
    }
}
