<?php

namespace App\Listeners;

use App\Events\MalformedRequest;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Rejection;
use Illuminate\Notifications\Notifiable;
use App\Notifications\RejectionNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MalformedRequestListener
{
    use Notifiable;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  MalformedRequest  $event
     * @return void
     */
    public function handle(MalformedRequest $event)
    {
        $this->notify(new RejectionNotification($event->rejection, $event->request));
    }

    /**
     * Route notifications for the Slack channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForSlack($notification)
    {
        return env('SLACK_NOTIFICATION_WEBHOOK_URL');
    }
}
