<?php

namespace App\Listeners;

use App\Events\EndPointSuccess;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Webhook;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class EndPointFailedSendWebhooks
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EndPointFailed  $event
     * @return void
     */
    public function handle(EndPointSuccess $event)
    {
        $webhooks = Webhook::where('event', '==', get_class($this))->get();
        if ($webhooks) {
            foreach ($webhooks as $webhook) {
                $client = new Client();
                $response = $client->request($webhook->method, $webhook->url, [
                    'body' => $event
                ]);
            }
        }
    }
}
