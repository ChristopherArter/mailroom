<?php

namespace App\Listeners;

use App\Events\EndPointJobFailure;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EndPointJobFailureListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EndPointJobFailure  $event
     * @return void
     */
    public function handle(EndPointJobFailure $event)
    {
        //
    }
}
