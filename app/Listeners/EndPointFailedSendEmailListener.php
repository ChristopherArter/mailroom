<?php

namespace App\Listeners;

use App\Events\EndPointFailed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\EndpointFailedNotificaiton;
use Illuminate\Notifications\Notifiable;
use App\Notifications\SlackMessage;

class EndPointFailedSendEmailListener
{
    use Notifiable;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EndPointFailed  $event
     * @return void
     */
    public function handle(EndPointFailed $event)
    {
        $this->notify(new EndpointFailedNotificaiton($event->responseRecord));
    }

    /**
     * Route notifications for the Slack channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForSlack($notification)
    {
        return env('SLACK_NOTIFICATION_WEBHOOK_URL');
    }
}
