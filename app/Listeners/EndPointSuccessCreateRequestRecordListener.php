<?php

namespace App\Listeners;

use App\Events\EndPointSuccess;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EndPointSuccessCreateRequestRecordListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EndPointSuccess  $event
     * @return void
     */
    public function handle(EndPointSuccess $event)
    {
        //
    }
}
