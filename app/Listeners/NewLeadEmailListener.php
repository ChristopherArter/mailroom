<?php

namespace App\Listeners;

use App\Events\NewLead;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class NewLeadEmailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewLead  $event
     * @return void
     */
    public function handle(NewLead $event)
    {
        try {
            $lead = $event->lead;
            $site = $lead->getSite();
            if ($site->isActive() && $site->email_distro) {
                Mail::send('emails.new_lead_notification', ['lead' => $lead], function ($m) use ($lead) {
                    $site = $lead->getSite();
                    $m->from('postmaster@sandbox98433.mailgun.org', 'MailRoom');
                    $m->to($site->email_distro)->subject('New Lead!');
                });
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
