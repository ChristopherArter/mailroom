<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EndPoint;
use App\Site;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class RequestRecord extends Model
{



    /**
     * Capture incoming request data
     * and map it to the request. This method
     * is used in the middleware.
     *
     * @param  $request
     * @return void
     */
    public function mapRequest($request)
    {
        $this->url = $request->fullUrl();
        $this->header = collect($request->headers->all())->toJson();
        $this->body = collect($request->all())->toJson();
        $this->method = $request->method();
        $this->referring_ip = (int) $request->ip();
        $this->raw = $request->toJson();
        return $this;
    }

    /**
     * Capture the response portion of the request.
     *
     * @param EndPoint $endPoint
     * @param Site $Site
     * @param [type] $response
     * @return void
     */
    public function saveResponse(EndPoint $endPoint, Site $Site, $response)
    {
        $newRequestRecord = $this->replicate();
        
        $newRequestRecord->response_code = (string) $response->getStatusCode();
        $newRequestRecord->end_point_id = $endPoint->id;
        $newRequestRecord->site_id = $Site->id;
        $newRequestRecord->response_body = collect((string) $response->getBody())->toJson();
        $newRequestRecord->save();
    }

    public function endPoint()
    {
        return $this->belongsTo('App\EndPoint');
    }

    public function Site()
    {
        return $this->belongsTo('App\Site');
    }
}
