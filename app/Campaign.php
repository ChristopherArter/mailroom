<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EndPoint;
use App\Lead;
use App\CampaignGroup;

class Campaign extends Model
{
    protected $table = 'campaigns';
    /**
     * Leads model relationship
     *
     * @return void
     */
    public function leads()
    {
        return $this->hasMany('App\Lead');
    }

    /**
     * EndPoint model relationship
     *
     * @return void
     */
    public function endPoints()
    {
        return $this->belongsToMany('App\EndPoint');
    }

    public function campaignGroup()
    {
        return $this->belongsTo('App\CampaignGroup');
    }
}
