<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EndPointSet;
use App\EndPoint;
use Illuminate\Http\Request;

class Site extends Model
{
    protected $table = 'sites';
    public $request;
    public $fillable = [
        'name',
        'domain',
        'api_key',
        'email_distro',
        'slack_channel',
        'slack_emoji',
        'slack_endpoint',
        'active',
        'test_email_distro',

    ];

    public function leads()
    {
        return $this->hasMany('App\Lead');
    }

    public function isActive()
    {
        return $this->active;
    }

    public function activate()
    {
        $this->active = true;
        $this->save();
    }

    public function deActivate()
    {
        $this->active = false;
        $this->save();
    }

    /**
     * Define the model relationship to the parent model of endpoints.
     *
     * @return void
     */
    public function endPointSets()
    {
        return $this->belongsTo('App\EndPointSet');
    }
    
    /**
     * Return all EndPoints for all EndPointSets.
     * This method is way too nested, and is
     * bad practice. This will be abstracted correctly
     * later on.
     *
     * @return void
     */
    public function endPoints(string $endPointSetId = null)
    {
        if ($endPointSets = $this->endPointSets) {
            $endPoints = collect();

            foreach ($endPointSets as $endPointSet) {
                if ($endPointSetId) {
                    if ($endPointSet->id == $endPointSetId) {
                        foreach ($endPointSet->endPoints as $endPoint) {
                            $endPoints->push($endPoint);
                        }
                    }
                } else {
                    if ($endPointSet->endPoints) {
                        foreach ($endPointSet->endPoints as $endPoint) {
                            $endPoints->push($endPoint);
                        }
                    }
                }
            }
            return $endPoints;
        }
        return false;
    }
}
