<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\RequestRecord;
use App\Site;
use App\EndPoint;
use App\Lead;
use App\Events\EndPointSuccess;
use App\Events\EndPointFailed;
use App\ResponseRecord;
use Exception;
use App\Events\EndPointJobFailure;
use Mail;
use App\Providers\Sender;

class ProcessRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $lead;
    public $endPoint;
    public $responseRecord;
    
    /**
     * Create a new job instance.
     * The data attribute is passed
     * by the static dispatch method
     * inside the SitesController,
     * which calls this job.
     *
     * @return void
     */
    public function __construct(Lead $lead, EndPoint $endPoint, ResponseRecord $responseRecord = null)
    {
        $this->lead = $lead;
        $this->endPoint = $endPoint;
        $this->responseRecord = $responseRecord;
    }

    /**
     * In order to maintain speed, this
     * que will process each outbound
     * request and save to the database.
     *
     * @return void
     */
    public function handle()
    {
        $this->lead->recordHistory('sent to endpoint', null, 'info');
        Sender::send($this->lead, $this->endPoint, $this->responseRecord->id);
    }
 

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $this->lead->recordHistory('endpoint send failed', null, 'failed');
        new EndPointJobFailure($exception);
    }
}
