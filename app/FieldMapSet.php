<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\FieldMap;
use App\EndPoint;

class FieldMapSet extends Model
{
    protected $with = ['fieldMaps'];

    public function fieldMaps()
    {
        return $this->hasMany('App\FieldMap');
    }

    public function endPoints()
    {
        return $this->hasMany('App\EndPoint');
    }

    public function duplicate($model)
    {
        if ($model instanceof FieldMap) {
            $this->duplicateFieldMapsFromFieldMapSet($model);
        } elseif ($model instanceof EndPoint) {
            $this->duplicateFieldMapsFromEndpoint($model);
        }
    }

    /**
     * translate the field_maps column to FieldMap objects
     * and assign them to this object.
     *
     * @param EndPoint $endPoint
     * @return void
     */
    public function duplicateFieldMapsFromEndpoint(EndPoint $endPoint)
    {
        if ($model->field_maps) {
            $newFieldMaps = [];

            foreach ($model->field_maps as $fieldMap) {
                $fieldMapModel = new FieldMap();
                $fieldMapModel->target = $fieldMap['target'];
                $fieldMapModel->output = $fieldMap['output'];
                $newFieldMaps[] = $fieldMapModel;
            }

            $this->fieldMaps()->saveMany($newFieldMaps);
        }
    }

    /**
     * Create new records for
     *
     * @param FieldMapSet $fieldMapSet
     * @return void
     */
    public function duplicateFieldMapsFromFieldMapSet(FieldMapSet $fieldMapSet)
    {
        $newFieldmaps = [];

        foreach ($fieldMapSet->fieldMaps as $fieldmap) {
            $newFieldMap = $fieldmap->replicate();
            $newFieldMaps[] = $newFieldMap;
        }

        $this->fieldMaps()->saveMany($newFieldMaps);
    }
}
