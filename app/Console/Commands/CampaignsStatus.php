<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Helper\TableCell;
use App\Campaign;
use App\EndPoint;

use Symfony\Component\Console\Formatter\OutputFormatterStyle;

class CampaignsStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campaigns:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $style = new OutputFormatterStyle('white', 'blue', ['bold']);
        $this->output->getFormatter()->setStyle('bigBlue', $style);
    
        $data = [];
        // Create a new Table instance.
        $table = new Table($this->output);

        // Set the table headers.
        $table->setHeaders([
        'ID', 'Name', 'URL', 'Status'
    ]);

        // Create a new TableSeparator instance.
        $separator = new TableSeparator;

        $campaigns = Campaign::all();
   
        foreach ($campaigns as $campaign) {
            if (!$campaign->endPoints->isEmpty()) {
                array_push($data, [ new TableCell('<bigBlue>'. $campaign->name .'</bigBlue>', ['colspan' => 4]) ]);
                array_push($data, new TableSeparator);

                foreach ($campaign->endPoints as $endPoint) {
                    array_push($data, [ $endPoint->id, $endPoint->name , $endPoint->url, ($endPoint->active) ? 'Active' : 'Disabled' ]);
                }
                array_push($data, new TableSeparator);
            }
        }



        // Set the contents of the table.
        $table->setRows($data);

        // Render the table to the output.
        $table->render();

    
//    // Create a new Table instance.
//    $table = new Table($this->output);

//    // Set the table headers.
//    $table->setHeaders([
//        'Campaigns (routes)'
//    ]);

//    // Create a new TableSeparator instance.
//    $separator = new TableSeparator;

//    // Set the contents of the table.

//    $data = [];

//    $table->setRows($data);

//    // Render the table to the output.
//    $table->render();
    }
}
