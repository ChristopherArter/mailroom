<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Site;

class DisableSite extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sites:disable {id?} {--all}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Disable a site or all sites.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('all')) {
            $sites = Site::all();
            $bar = $this->output->createProgressBar(count($sites));

            foreach ($sites as $site) {
                $site->deActivate();
                $bar->advance();
            }
            $bar->finish();
            
            $this->info("\n" . 'All sites disabled, my dudes');
            $this->call('sites:status');
        } elseif ($this->argument('id')) {
            $site = Site::find($this->argument('id'));
            $site->deActivate();
        }
    }
}
