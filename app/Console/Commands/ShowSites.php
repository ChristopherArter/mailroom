<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Site;

class ShowSites extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sites:status {id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get the status of all sites';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $headers = ['ID','Name', 'Domain', 'Email Distro', 'Test Email Distro', 'Slack Channel', 'Active'];

        if ($this->argument('id')) {
            $sites = Site::where('id', $this->argument('id'))->select([
                'id',
                'name',
                'domain',
                'email_distro',
                'test_email_distro',
                'slack_channel',
                'active'
    
                ])->get()->toArray();
        } else {
            $sites = Site::all([
                'id',
                'name',
                'domain',
                'email_distro',
                'test_email_distro',
                'slack_channel',
                'active'
    
                ])->toArray();
        }
        
        $this->table($headers, $sites);
    }
}
