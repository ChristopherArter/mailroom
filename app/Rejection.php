<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use App\Notifications\RejectionNotification;

class Rejection extends Model
{
    use Notifiable;

    public function reject(Request $request, $type, $reason, $requestId = null, $error = null)
    {
        $this->type = $type;
        $this->reason = $reason;
        $this->request_id = $requestId;
        $this->save();
        $this->notify(new RejectionNotification($this, $request));
    }

    /**
     * Route notifications for the Slack channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForSlack($notification)
    {
        return env('SLACK_NOTIFICATION_WEBHOOK_URL');
    }
}
