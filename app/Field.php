<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\FieldSet;
use DB;

class Field extends Model
{
    protected $table = 'fields';

    public function fieldSet()
    {
        return $this->belongsTo('App\FieldSet');
    }

    public function isValid($fieldName)
    {
        // later this will be checked against a list in the database
        return true;
    }
}
