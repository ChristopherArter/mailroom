<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

        'App\Events\EndPointFailed' => [
            'App\Listeners\EndPointFailedSendEmailListener',
            'App\Listeners\EndPointFailedLogListener'
            //'App\Listeners\EndPointFailedSendWebhooks'
        ],
        'App\Events\EndPointSuccess' => [
            'App\Listeners\EndPointSuccessCreateRequestRecordListener'
            //'App\Listeners\EndPointFailedSendWebhooks'
        ],
        'App\Events\MalformedRequest' => [
            'App\Listeners\MalformedRequestListener'
        ],

        'App\Events\EndPointJobFailure' => [
            'App\Listeners\EndPointJobFailureListener'
        ],

        'App\Events\NewLead' => [
            'App\Listeners\NewLeadEndPointListener',
            'App\Listeners\NewLeadEmailListener',
            'App\Listeners\NewLeadSlackListener'
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
