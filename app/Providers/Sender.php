<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\RequestRecord;
use App\Site;
use App\EndPoint;
use App\Lead;
use App\Events\EndPointSuccess;
use App\Events\EndPointFailed;
use App\ResponseRecord;
use Exception;
use App\Events\EndPointJobFailure;
use Mail;
use App\FieldMap;

class Sender extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function send(Lead $lead, EndPoint $endPoint, ResponseRecord $responseRecord = null)
    {

        // filter the lead input
        $lead->fields = $endPoint->filterFields($lead);

        if ($endPoint->isEmail()) {
            $newResponse = self::sendEmail($lead, $endPoint, $responseRecord);
        } else {
            $newResponse = self::sendToUrl($lead, $endPoint, $responseRecord);
        }
        return $newResponse;
    }

    /**
     * In order to maintain speed, this
     * que will process each outbound
     * request and save to the database.
     *
     * @return void
     */

    public static function sendToUrl(Lead $lead, EndPoint $endPoint, ResponseRecord $responseRecord = null)
    {
        $guzzleClient = new Client();

        // set fields and http_errors
        $postOptions = [
            'form_params' => $endPoint->prepareFields($lead),
            'http_errors' => false
        ];

        // set headers
        $postOptions = $endPoint->setHeadersForRequest($postOptions);

        // make the call
        $response = $guzzleClient->post($endPoint->url, $postOptions);

        $responseRecord = self::resolveResponseRecord($lead, $responseRecord);
        $responseRecord->saveResponse($endPoint, $response);
        $lead->delivery_status = $responseRecord->getResponseStatus();
        $lead->save();

        return $responseRecord;
    }


    /**
     * Returns an existing response record
     * if set, or a new response record if
     * it is not set.
     *
     * @return ResponseRecord
     */
    protected static function resolveResponseRecord(Lead $lead, ResponseRecord $responseRecord = null)
    {
        if ($responseRecord) {
            return $responseRecord;
        } else {

            // Create new response record.
            $responseRecord = new ResponseRecord();
            $responseRecord->lead_id = $lead->id;
            return $responseRecord;
        }
    }
    
    /**
     * If it is an email end point,
     * let's handle sending that in
     * this method.
     *
     * @return void
     */
    protected static function sendEmail(Lead $lead, EndPoint $endPoint, ResponseRecord $responseRecord = null)
    {
        try {
            Mail::send('emails.new_lead', ['lead' => $lead, 'fields' => $endPoint->prepareFields($lead) ], function ($m) use ($lead, $endPoint) {
                $site = $lead->getSite();
                $m->from(config('services.mailgun.from_email'), config('services.mailgun.from_name'));
                $m->to($endPoint->url)->subject('New Lead!');
            });

            $responseRecord = self::resolveResponseRecord($lead, $responseRecord);
            $responseRecord->successEmailResponse($endPoint);
        } catch (GuzzleException $e) {
            $responseRecord = self::resolveResponseRecord($lead, $responseRecord);
            $responseRecord->saveResponse($endPoint, $e->getResponse());
        }
        
        return $responseRecord;
    }
}
