<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Field;

class FieldSet extends Model
{
    public function endPoints()
    {
        return $this->belongsToMany('App\Endpoint', 'field_set_end_point', 'end_point_id', 'field_set_id');
    }

    public function fields()
    {
        return $this->hasMany('App\Field');
    }
}
