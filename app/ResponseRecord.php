<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EndPoint;
use App\Site;
use App\Lead;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Events\EndPointSuccess;
use App\Events\EndPointFailed;
use App\Providers\Sender;
use App\Jobs\SetLeadResponseStatus;

class ResponseRecord extends Model
{

    /**
     * Success codes for check methods.
     *
     * @var array
     */
    protected $successCodes;
    
    /**
     * Table for model
     *
     * @var string
     */
    protected $table = 'response_records';


    /**
     * Constructor to set success codes.
     *
     * @param array $successCodes
     */
    public function __construct(array $successCodes = null, array $attributes = array())
    {
        parent::__construct($attributes);

        if (!$successCodes) {
            $this->successCodes = [
                200,
                201,
                202,
                203,
                204,
                205,
                206,
                207,
                208,
                226
            ];
        } else {
            $this->successCodes = $successCodes;
        }
    }


    /**
     * Capture the response from the
     * endpoint send.
     *
     * @param EndPoint $endPoint
     * @param Site $Site
     * @param $response
     * @return void
     */
    public function saveResponse(EndPoint $endPoint, $response)
    {
        dump($response);
        $this->request = $response;
        $this->response_code = $response->getStatusCode();
        $this->end_point_id = $endPoint->id;
        $this->reason_phrase = $response->getReasonPhrase();
        $this->response_body = $response->getBody()->getContents();
        $this->save();
        $this->sendEvents();
        return $this;
    }
    
    /**
     * Save a successful email response;
     *
     * @param EndPoint $endPoint
     * @return void
     */
    public function successEmailResponse(EndPoint $endPoint)
    {
        $this->request = collect(['foo'=>'bar']);
        $this->response_code = '200';
        $this->end_point_id = $endPoint->id;
        $this->reason_phrase = 'great success!';
        $this->response_body = collect();
        $this->save();
        $this->sendEvents();
        return $this;
    }
    /**
     * Save the data from a failed email send.
     * This is pretty tricky, and only relying
     * on a try / catch. This may not be
     * fool-proof.
     *
     * @param EndPoint $endPoint
     * @param $error
     * @return void
     */
    public function failedEmailResponse(EndPoint $endPoint, $error = null)
    {
        $this->response_code = 500;
        $this->response_body = $error;
        $this->end_point_id = $endPoint->id;
        $this->reason_phrase = "Email failed";
        $this->request = collect(['foo'=>'bar']);
        $this->save();
        $this->sendEvents();
        return $this;
    }


    /**
     * This method determines if
     * a request response was successful
     *
     * @param $response
     * @param RequestRecord $requestRecord
     * @return void
     */
    public function success()
    {
        if (in_array($this->response_code, $this->successCodes)) {
            return true;
        }
        return false;
    }
    
    public function getResponseStatus()
    {
        if ($this->success()) {
            return 'success';
        }

        if (! $this->success()) {
            return 'failed';
        }
    }


    /**
     * Send events based on if this
     * request succeeded or failed
     *
     * @param  $response
     * @return void
     */
    public function sendEvents()
    {
        if ($this->success()) {
            $this->lead->recordHistory('endpoint success', null, 'success');

            event(new EndPointSuccess($this));
        } else {
            $this->lead->recordHistory('endpoint failed', null, 'failed');

            event(new EndPointFailed($this));
        }
    }

    /**
     * Refire response record.
     *
     * @return void
     */
    public function refire()
    {
        Sender::send($this->lead, $this->endPoint, $this);
        SetLeadResponseStatus::dispatch($this->lead);
    }

    /**
     * Lead model relationship
     *
     * @return void
     */
    public function lead()
    {
        return $this->belongsTo('App\Lead');
    }

    /**
     * Get whether this was successful or failed.
     *
     * @return void
     */
    public function getSuccessAttribute()
    {
        return $this->success();
    }

    public function endPoint()
    {
        return $this->belongsTo('App\EndPoint');
    }
}
