<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Site;
use App\Lead;
use App\ResponseRecord;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\RequestRecord;
use App\Jobs\ProcessRequest;
use App\EndPointSet;
use App\FieldMap;
use App\FieldMapSet;

class EndPoint extends Model
{
    protected $table = 'end_points';

    protected $casts = [
        'headers'           => 'array',
        'filtered_fields'   =>  'array',
        'field_maps'        =>  'array'
    ];
    protected $with = ['campaigns','fieldMapSet'];

    /**
     *  This model represents endpoints
     * that one source can hit. One request
     * can pass it's values to many endpoints.
     * The fieldset relationship is a set of fields that
     * can be used for different types of endpoints
     * in the application.
     */


    /**
     * Checks if this endPoint is an email address
     *
     * @return boolean
     */
    public function isEmail()
    {
        return filter_var($this->url, FILTER_VALIDATE_EMAIL);
    }


    /**
     *  ELOQUENT RELATIONSHIPS
     */

    /**
     * Sites eloquent relationship
     *
     * @return void
     */
    public function sites()
    {
        return $this->belongsToMany('App\Site', 'site_end_point', 'end_point_id', 'site_id');
    }

    /**
     * Campaign eloquent relationship
     *
     * @return void
     */
    public function campaigns()
    {
        return $this->belongsToMany('App\Campaign', 'campaign_end_point', 'end_point_id', 'campaign_id');
    }

    /**
     * EndPointSet eloquent relationship
     *
     * @return void
     */
    public function endPointSets()
    {
        return $this->belongsToMany('App\endPointSet', 'end_points_end_point_sets');
    }

    /**
     * FieldMapSet eloquent relationship.
     *
     * @return void
     */
    public function fieldMapSet()
    {
        return $this->belongsTo('App\FieldMapSet');
    }



    /**
     * ResponseRecord eloquent relationship.
     *
     * @return void
     */
    public function responseRecords()
    {
        return $this->hasMany('App\ResponseRecord');
    }


    /**
     * Combines the end point data,
     * the lead fields, and a response record
     * into a job and sends.
     *
     * @param Lead $lead
     * @param ResponseRecord $responseRecord
     * @return void
     */
    public function send(Lead $lead, ResponseRecord $responseRecord = null)
    {
        ProcessRequest::dispatch($lead, $this, $responseRecord);
    }

    /**
     * This creates a filters out only certain
     * fields to be included.
     *
     * @param Lead $lead
     * @return void
     */
    public function filterFields(Lead $lead)
    {
        if ($filteredFields = $this->filtered_fields) {
            $filteredFieldsCollection = collect();
            foreach ($lead->fields as $key => $value) {
                if (in_array($key, $filteredFields)) {
                    $filteredFieldsCollection->put($key, $value);
                }
            }
            return $filteredFieldsCollection->toArray();
        } else {
            return $lead->fields;
        }
    }

    /**
     * This method is the pipeline that first
     * filters the fields, then maps them if
     * the endpoint has field maps.
     *
     * @param Lead $lead
     * @return void
     */
    public function prepareFields(Lead $lead)
    {
        $fields = $this->filterFields($lead);
        $fields = $this->mapToFields($lead);
        return $fields;
    }

    /**
     * Map lead data to fieldmaps.
     *
     * @param [type] $lead
     * @return void
     */
    public function mapToFields($lead)
    {
        $fields = $lead->fields;

        // has a field map set
        if ($this->field_map_template && $this->fieldMapSet && $this->fieldMapSet->fieldMaps) {
            foreach ($this->fieldMapSet->fieldMaps as $fieldMap) {
                if (array_key_exists($fieldMap->target, $fields)) {
                    $value = $fields[$fieldMap->target];
                    unset($fields[$fieldMap->target]);
                    $fields[$fieldMap->output] = $value;
                }
            }
        } elseif ($fieldMaps = $this->field_maps) {
            foreach ($fieldMaps as $fieldMap) {
                if (array_key_exists($fieldMap['target'], $fields)) {
                    $value = $fields[$fieldMap['target']];
                    unset($fields[$fieldMap['target']]);
                    $fields[$fieldMap['output']] = $value;
                }
            }
        }

        return $fields;
    }


    /**
     * Prepares the request headers.s
     *
     * @param array $requestOptions
     * @return void
     */
    public function setHeadersForRequest(array $requestOptions)
    {
        // set the headers of the endpoint model, if set.
        if ($this->headers) {
            $requestHeaders = [];
                
            foreach ($this->headers as $header) {
                array_push($requestHeaders, [ $header['key'] => $header['value'] ]);
            }
            $requestOptions['headers'] = $requestHeaders;
        }

        return $requestOptions;
    }

    /**
     *  ATTRIBUTES & MUTATORS
     */

    public function getNameAttribute($value)
    {
        return $value;
    }
}
