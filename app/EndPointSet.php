<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class EndPointSet extends Model
{
    public function endPoints()
    {
        return $this->belongsToMany('App\EndPoint', 'end_points_end_point_sets');
    }

    public function attachEndPoints(Request $request)
    {
        if ($request->input('end_points')) {
            foreach ($request->input('end_points') as $endPoint) {
                $this->endPoints()->sync($endPoint['id']);
            }
        }
    }
}
