<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use App\Rejection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\Lead;
use App\Campaign;
use App\Site;
use App\Config;

class RejectionNotification extends Notification
{
    use Queueable;

    public $rejection;
    public $request;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Rejection $rejection, Request $request)
    {
        $this->rejection = $rejection;
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable)
    {
        $rejection = $this->rejection;
        $request = $this->request;
        
        $site = Site::find($request->input(config('app.fields.site-key')));

        $campaign = Campaign::where('name', $request->input('RUID'))->first();
    

        $carbon = new Carbon($this->rejection->created_at);
        $carbon->subHours(4);
        $slackTitle = '[MailRoom] :x: Request Rejected';
        $channelIcon = [
            'web'       =>  ':desktop_computer:',
            'call'      =>  ':telephone:',
            'chat'      =>  ':speech_balloon:',
            'facebook'  =>  ':facebook:'
        ];

        if (array_key_exists($request->input('channel'), $channelIcon)) {
            $icon = $channelIcon[$request->input('channel')];
        } else {
            $icon = ':question:';
        }
        return (new SlackMessage)
        ->from('MailRoom', ':postbox:')
        ->to('lead-error')
        ->warning()

        ->attachment(function ($attachment) use ($rejection, $request, $campaign, $carbon, $icon, $slackTitle, $site) {
            $attachment->title($slackTitle)
                       ->fields([
                            'Reason'    =>  $rejection->reason,
                            'Type'      =>  ucwords($rejection->type),
                           'Date'       =>  ':spiral_calendar_pad: ' . $carbon->toFormattedDateString(),
                           'Time'       =>  ':clock1: ' . $carbon->format('h:i A'),
                           'Site'       =>  (isset($site->name)) ? $site->name : 'Unknown',
                           'Channel'   =>  $icon . ' '. ucwords($request->input('channel')),
                            'Route'     =>  (isset($campaign->name)) ? $campaign->name : 'Unkown'
                           

                       ]);
        });
    }
}
