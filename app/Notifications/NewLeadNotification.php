<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\Lead;
use App\Campaign;
use App\Site;
use App\Config;

class NewLeadNotification extends Notification
{
    use Queueable;
    public $lead;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Lead $lead)
    {
        $this->lead = $lead;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('You got a new lead dummy!.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable)
    {
        $lead = $this->lead;
        $url = route('leads.show', $this->lead->id);
        $carbon = new Carbon($lead->created_at);
        $carbon->subHours(4);
        $lead->lead_campaign = Campaign::find($lead->campaign_id);

        if ($lead->isTest()) {
            $slackChannel = config('app.test_slack_channel');
            $slackTitle = '[TEST] [MailRoom] :incoming_envelope: New Lead!';
        } else {
            $slackChannel = $lead->getSite()->slack_channel;
            $slackTitle = '[MailRoom] :incoming_envelope: New Lead!';
        }
        
        $channelIcon = [
            'web'   =>  ':desktop_computer:',
            'call' =>  ':telephone:',
            'chat'  =>  ':speech_balloon:',
            'facebook'  =>  ':facebook:'
        ];

        if (array_key_exists($lead->channel, $channelIcon)) {
            $icon = $channelIcon[$lead->channel];
        } else {
            $icon = ':question:';
        }
        $lead->site = $lead->getSite();
        return (new SlackMessage)
        ->from('MailRoom', ':postbox:')
        ->to($slackChannel)
        ->success()

        ->attachment(function ($attachment) use ($lead, $carbon, $icon, $slackTitle) {
            $attachment->title($slackTitle, route('leads.show', $this->lead->id))
                       ->fields([

                           'Date'       =>  ':spiral_calendar_pad: ' . $carbon->toFormattedDateString(),
                           'Time'       =>  ':clock1: ' . $carbon->format('h:i A'),
                           'Site'       => ucwords($lead->site->name),
                           'Channel'   =>  $icon . ' '. ucwords($lead->channel),

                       ]);
        });
    }
}
