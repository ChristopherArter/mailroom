<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\ResponseRecord;
use App\Lead;
use App\Campaign;
use App\Site;

class EndpointFailedNotificaiton extends Notification
{
    use Queueable;
    public $responseRecord;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(ResponseRecord $responseRecord)
    {
        $this->responseRecord = $responseRecord;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }


    public function getResponseRecord()
    {
        return $this->responseRecord;
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable)
    {
        $responseRecord = $this->responseRecord;

        $responseRecord->lead = Lead::find($responseRecord->lead_id);
        $responseRecord->campaign = Campaign::find($responseRecord->lead->campaign_id);
        $responseRecord->site = Site::find($responseRecord->campaign->site_id);
        $carbon = new Carbon($responseRecord->created_at);
        $carbon->subHours(4);
        $lead = $responseRecord->lead;
        $site = $lead->getSite();
        $slackChannel = $site->slack_channel;

        return (new SlackMessage)
        ->from('MailRoom', ':postbox:')
        ->to($slackChannel)
        ->warning()
        ->attachment(function ($attachment) use ($responseRecord) {
            $attachment->title('MailRoom: :warning: Delivery failure!', route('pages.monitor'))
                       ->fields([
                           'Response Code'  =>  $responseRecord->response_code,
                           'Reason'         =>  $responseRecord->reason_phrase,
                           'Date'           =>  'foo',
                           'Time'           =>  'bar',
                           'Lead'           =>  route('leads.show', $responseRecord->lead->id),
                           'Campaign'       =>  $responseRecord->campaign->name

                       ]);
        });
    }
}
