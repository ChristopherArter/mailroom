<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Code;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\BelongsTo;
use Fourstacks\NovaRepeatableFields\Repeater;

use Laravel\Nova\Http\Requests\NovaRequest;

class EndPoint extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\EndPoint';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name',
    ];
    public static function label()
    {
        return 'Endpoints';
    }
    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('name')->sortable(),
            Boolean::make('active'),
            BelongsTo::make('Field Map Set', 'fieldMapSet'),
            Boolean::make('Use Field Maps', 'field_map_template')
                ->hideFromIndex()
                ->help('Toggle if you want to use a field mapping template'),
            BelongsToMany::make('Campaigns'),
            
            Date::make('Created', 'created_at')->sortable(),
            Date::make('Updated', 'updated_at')->hideFromIndex(),
            Text::make('url')->hideFromIndex(),
            Text::make('token')->hideFromIndex(),
            Code::make('Headers')->json(),
            Code::make('Field Filters', 'filtered_fields')->help('Limit to just these fields')->json(),
            
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
