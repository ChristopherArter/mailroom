<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignGroup extends Model
{
    public function attachCampaigns(Request $request)
    {
        if ($request->input('campaigns')) {
            foreach ($request->input('campaigns') as $campaign) {
                $this->campaigns()->sync($campaign['id']);
            }
        }
    }

    public function campaigns()
    {
        return $this->hasMany('App\Campaign');
    }
}
