<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Campaign;
use App\Site;
use Illuminate\Http\Request;
use App\Endpoint;
use Illuminate\Support\Facades\Log;
use App\EndPointSet;
use App\Jobs\ProcessRequest;
use App\Jobs\SetLeadResponseStatus;
use App\Providers\Sender;
use App\ResponseRecord;
use App\Events\NewLead;
use App\Tracker;

class Lead extends Model
{
    protected $casts = [
            'fields'    => 'array',
            'history'   =>  'array',
            'request'   =>  'array',
        ];
        
    protected $table = 'leads';
    protected $with = ['campaign', 'responseRecords'];
    protected $appends = ['campaign_name', 'responses_status', 'shuttle_success'];

    public function __construct($attributes = array())
    {
        parent::__construct($attributes);
        $this->recordHistory('Mail Room lead created', null, 'success', false);
    }

    /**
     * The inbound request as a param.
     *
    * @var Request
     */
    public $request;


    /**
     * This method is responsible for the flow of filling
     * out the lead object.
     *
     * @param Request $request
     * @return void
     */
    public function setValuesFromRequest(Request $request)
    {
        try {
            $this->request = $request->all();
            $this->setCampaign($request);
            $this->setFields($request);
            $this->setSite($request);
            $this->setLuid($request);
            $this->setChannel($request);
        } catch (Exception $e) {
            return $e;
        }
    }

        
    /**
     * Sets the LUID for the request.
     * The LUID is a unique number that
     * corresponds with pipethat and also corresponds
     * the ID of the raw request in the read-only
     * database of inbound requests.
     *
     * @param Request $request
     * @return void
     */
    public function setLuid(Request $request)
    {
        try {
            // remove PT from beginning
            $this->luid = substr((string) $request->input('fields')['luid'], 2);

            // Remove the zeros.
            $this->luid = ltrim($this->luid, '0');
        } catch (Exception $e) {
            return $e;
        }

        return $this;
    }

    /**
     * Set the site model relationship
     *
     * @param Request $request
     * @return void
     */
    public function setSite(Request $request)
    {
        $this->site_id = $request->input(config('app.fields.site-key'));
        return $this;
    }

    /**
     * Temporary workaround until
     * eloquent relationships issue is
     * diagnosed.
     *
     * @return void
     */
    public function getSite()
    {
        return Site::find($this->site_id);
    }

    /**
     * Set the channel of the lead.
     *
     * @param Request $request
     * @return void
     */
    public function setChannel($request)
    {
        $this->channel = $request->input('channel', '');
        return $this;
    }
        
    /**
     * Set the campaign model relationship
     *
     * @param Request $request
     * @return $this
     */
    public function setCampaign(Request $request)
    {
        $campaign = Campaign::where('name', $request->input('fields')['ruid'])->first();

        $this->campaign_id = $campaign->id;
    }

        
    /**
     * get the campaign for this model
     *
     * @return void
     */
    public function getCampaign()
    {
        return Campaign::find($this->campaign_id);
    }



    /**
     * Set the lead fields
     *
     * @param Request $request
     * @return void
     */
    public function setFields(Request $request)
    {
        $fields = $request->input('fields');
        $this->fields = $request->input('fields');
    }

    /**
     * Set the status of the shuttle by recording an event.
     *
     * @param Request $request
     * @return void
     */
    public function setShuttleStatus(Request $request)
    {
        if (! $request->input('shuttle_id')) {
            $this->recordHistory('shuttle failed', null, 'failed');
        }
    }

    /**
     * return endpoints for Lead
     *
     * @return collection
     */
    public function getEndPoints()
    {
        $campaign = $this->getCampaign();
        return $campaign->endPoints;
    }


    /**
     * Get the campaign as a string.
     *
     * @return string
     */
    public function getCampaignNameAttribute()
    {
        //$campaign = $this->getCampaign();
        if ($this->campaign) {
            return $this->campaign->name;
        }
    }

 
        
    /**
     * Get the lead delivery results as a string.
     *
     * @return string
     */
    public function getResponsesStatusAttribute()
    {
        // if (! $this->getFailedResponses()->isEmpty()) {
        //     return 'failed';
        // }

        // if ($this->responseRecords()->count() > 0) {
        //     return 'success';
        // }

        return $this->delivery_status;
    }

    /**
     *  SENDING ENDPOINT LEADS
     */

    /**
     * Send request to all
     * end points. Only sends
     * if the sites are active.
     *
     * @return void
     */
    public function sendToAllEndPoints($endPoints = null, $responseRecord = null)
    {
        if (!$endPoints) {
            $endPoints = $this->getEndPoints();
        }

        if ($this->getSite()->isActive() && ! $endPoints->isEmpty()) {
            foreach ($endPoints as $endPoint) {
                $this->sendToEndPoint($endPoint, $this, $responseRecord);
            }
        }

        SetLeadResponseStatus::dispatch($this);
    }

    /**
     * Send a lead's data
     * to the end point.
     *
     * @param $endPoint
     * @param Lead $lead
     * @return void
     */
    public function sendToEndPoint($endPoint, Lead $lead, ResponseRecord $responseRecord = null)
    {
        if ($endPoint->active == true) {
            $endPoint->send($lead, $responseRecord);
        }
    }


    /**
     * Get endpoints that have failed
     * response records.
     *
     * @return void
     */
    public function getFailedEndPoints()
    {
        $failedEndPoints = collect();

        if ($failedResponses = $this->getFailedResponses()) {
            foreach ($failedResponses as $failedResponse) {
                $failedEndPoints->push($failedResponse->endPoint);
            }
        }
        return $failedEndPoints;
    }


    /**
     * Get all failed response records of a lead.
     *
     * @return void
     */
    public function getFailedResponses()
    {
        $failedResponses = collect();

        foreach ($this->responseRecords as $responseRecord) {
            if (! $responseRecord->success()) {
                $failedResponses->push($responseRecord);
            }
        }
        return $failedResponses;
    }


    /**
     * Re-send the lead to the end points.
     *
     * @return void
     */
    public function refireFailedEndPoints()
    {
        $failedEndPoints = collect();

        if (! $this->getFailedResponses()->isEmpty()) {
            $failedResponses = $this->getFailedResponses();
            foreach ($failedResponses as $failedResponse) {
                $failedResponse->refire();
            }
        }
    }

        
    /**
     * Set the delivery status of the lead based
     * on all the endpoints response records.
     *
     * @return void
     */
    public function setDeliveryStatus()
    {
        if ($this->responseRecords->isEmpty()) {
            $this->delivery_status = 'unsent';
        } else {
            if ($this->getFailedResponses()->isEmpty()) {
                $this->delivery_status = 'success';
            } else {
                $this->delivery_status = 'failed';
            }
        }

        $this->save();
    }

        
    /**
     * Add an event in the lead's history.
     *
     * @param string $event
     * @param mixed $data
     * @param string $type
     * @param boolean $save
     * @return void
     */
    public function recordHistory(string $event, $data = null, $type = null, $save = true)
    {
        $this->history = Tracker::record($event, $this, $data, $type);

        if ($save) {
            $this->save();
        }
    }

    /**
     * Return all history, or specific item by event column
     *
     * @param string $event
     * @return void
     */
    public function getHistory(string $event = null)
    {
        if (!$event) {
            return $this->history;
        } else {
            return $this->getHistoryByEvent($event);
        }
    }

    /**
     * Get a specific historical item.
     *
     * @param string $event
     * @return void
     */
    protected function getHistoryByEvent($event)
    {
        if ($this->history && is_array($this->history)) {
            foreach ($this->history as $item) {
                if (isset($item['event']) && $item['event'] == $event) {
                    return $item;
                }
            }
        }
    }


    /**
     * Send a notification of a new lead
     *
     * @return void
     */
    public function notify()
    {
        event(new NewLead($this));
    }



    /**
     *  ELOQUENT RELATIONSHIPS
     *
     */


    /**
     * Site relationship
    *
    * @return void
    */
    public function site()
    {
        return $this->hasOne('App\Site');
    }

    /**
     * Campaign model relationship
     *
     * @return void
     */
    public function campaign()
    {
        return $this->belongsTo('App\Campaign');
    }

    /**
     * Response Record model relationship
     *
     * @return void
     */
    public function responseRecords()
    {
        return $this->hasMany('App\ResponseRecord');
    }

    /**
     *   MODEL ATTRIBUTES
     */

    /**
     * sets an attribute on model to return
     * failed endpoints.
     *
     * @return void
     */
    public function getFailedEndPointsAttribute()
    {
        return $this->getFailedEndPoints();
    }


    /**
     * Return boolean mutator for
     * $this->sent_successfully.
     *
     * @return void
     */
    public function getSentSuccessAttribute()
    {
        return $this->sentSuccessfully();
    }

    /**
     * Set the shuttle_sucess attribute
     *
     * @return void
     */
    public function getShuttleSuccessAttribute()
    {
        if ($this->getHistory('shuttle failed')) {
            return false;
        }
        return true;
    }

    /**
     *   HELPERS
     */

    /**
     * Determine if this is a test
     * Lead or not.
     *
     * @return boolean
     */
    public function isTest()
    {
        if ($this->campaign_id == config('app.test_campaign_id')) {
            return true;
        }
    }
}
