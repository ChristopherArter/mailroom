<?php

namespace App\Http\Controllers;

use App\Lead;
use Illuminate\Http\Request;
use App\Site;
use DB;
use App\Events\NewLead;
use App\Exceptions\Handler;
use App\Tracker;
use App\Rejection;
use App\Campaign;

class LeadController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lead = new Lead();
        
        try {
            $lead->setValuesFromRequest($request);
            $lead->save();
            $lead->setShuttleStatus($request);
            
            if (! $lead->isTest()) {
                $lead->sendToAllEndPoints();
            }
            
            $lead->notify();
        } catch (Exception $e) {
            $rejection = new Rejection();
            $rejection->reject($request, 'controller: LeadController@store', 'Failed to create lead', $request->input('request_id', ''), $e);
            return response($e->getMessage(), 500)
            ->header('Content-Type', 'text/plain');
        }

        return $lead;
    }

    // /**
    //  * Display a listing of the resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function index($count = null)
    // {
    //     if (!$count) {
    //         return Lead::all();
    //     } else {
    //         return Lead::orderBy('created_at', 'desc')->take((int) $count)->get();
    //     }
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $leads = Lead::orderBy($request->query('order_by', 'created_at'), $request->query('sort_direction', 'desc'));
            
        // find lead by LUID
        if ($request->query('luid')) {
            $leads->where('luid', 'like', '%' . $request->query('luid') . '%');
        }

        // find lead by campaign name
        if ($request->query('campaign')) {
            $campaign = Campaign::where('name', $request->query('campaign'))->first();
            if ($campaign) {
                $leads->where('campaign_id', $campaign->id);
            }
        }

        // delivery status param
        if ($request->query('delivery_status')) {
            $leads->where('delivery_status', $request->query('delivery_status'));
        }

        $leads = $leads->paginate($request->query('per_page', 20));

        return $leads;
    }


    public function indexByIds()
    {
        $leads = collect();
        foreach (Lead::all() as $lead) {
            $leads->push($lead->id);
        }
        return $leads;
    }

    /**
     * This method handles the query-change event object
     * passed by the Element-UI table in LeadsTable.
     * This event has filters property that is translated
     * to an eloquent query below.
     *
     * @param  $siteId
     * @param Request $request
     * @return void
     */
    public function query($siteId, Request $request)
    {

        // narrow the site down to make the subsiquent queries a bit faster.
        $leads = Lead::where('site_id', $siteId);

        if ($filters = $request->input('filters')) {
            foreach ($filters as $filter) {
                if (array_key_exists('date_range', $filter) && $filter['date_range']) {
                    $leads->whereDate('created_at', '>', $filter['date_range'][0]);
                    $leads->whereDate('created_at', '<', $filter['date_range'][1]);
                }

                // it isn't a date range.
                if (array_key_exists('value', $filter) && $filter['value']) {
                    if (is_array($filter['value'])) {

                            // it's the campaign array.
                        if ($filter['prop'] == 'campaign_id') {
                            $leads->whereIn('campaign_id', $filter['value']);
                        }

                        // it's the campaign array.
                        if ($filter['prop'] == 'responses_status') {
                            $leads->whereIn('delivery_status', $filter['value']);
                        }
                    } else {
                        $leads->where($filter['prop'], 'like', '%'. $filter['value'] . '%');
                    }
                }
            }
        }
        return $leads->orderBy('created_at', 'desc')->paginate(20);
    }

    /**
     * Get all leads but if a site id is passed,
     * limit the leads just to that.
     *
     * @param [type] $siteId
     * @return void
     */
    public function showWithSite($siteId = null)
    {
        if ($siteId) {
            // Site::find($siteId)->leads;
            return Lead::where('site_id', $siteId)->paginate(20);
        //return Site::find($siteId)->leads->paginate(20);
        } else {
            $leads = Lead::paginate(20);
            return $leads;
        }
    }


    /**
     * Resend leads that are either
     * unsent or have failed response
     * records.
     *
     * @param Request $request
     * @return void
     */
    public function resendLeads(Request $request)
    {
        foreach ($request->all() as $lead) {
            $lead = Lead::find((int) $lead['id']);

            if (! $lead->responseRecords->isEmpty()) {
                foreach ($lead->responseRecords as $responseRecord) {
                    $responseRecord->delete();
                }
            }

            $lead->recordHistory('refired', null, 'info');
            $lead->sendToAllEndPoints();
        }
    }

    /**
     * Return the latest lead
     *
     * @return void
     */
    public function latest(Request $request, $id = false)
    {
        return Lead::first();
    }

    /**
     * Return the latest lead ID
     *
     * @return void
     */
    public function latestId(Request $request, $id = false)
    {
        $lead = DB::table('leads')->select('id')->orderBy('created_at', 'desc')->take(1)->get();
        return $lead[0]->id;
    }

    public function leadHistory($id)
    {
        $lead = Lead::find($id);
        return $lead->history;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Lead::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function edit(Lead $lead)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lead $lead)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lead $lead)
    {
        //
    }
}
