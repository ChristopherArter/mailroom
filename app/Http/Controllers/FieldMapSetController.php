<?php

namespace App\Http\Controllers;

use App\FieldMapSet;
use App\FieldMap;
use Illuminate\Http\Request;

class FieldMapSetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fieldMapSets = FieldMapSet::with('fieldMaps')->get();
        return $fieldMapSets->load('endPoints');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FieldSet  $fieldSet
     * @return \Illuminate\Http\Response
     */
    public function show(FieldSet $fieldSet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FieldSet  $fieldSet
     * @return \Illuminate\Http\Response
     */
    public function edit(FieldSet $fieldSet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FieldSet  $fieldSet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FieldSet $fieldSet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FieldSet  $fieldSet
     * @return \Illuminate\Http\Response
     */
    public function destroy(FieldSet $fieldSet)
    {
        //
    }

    public function sync(Request $request)
    {
        if ($request->input('id')) {
            $fieldMapSet = FieldMapSet::find($request->input('id'));
        } else {
            $fieldMapSet = new FieldMapSet();
        }

        $fieldMapSet->name = $request->input('name');
        $fieldMapSet->save();

        if ($request->input('field_maps') && is_array($request->input('field_maps'))) {
            foreach ($request->input('field_maps') as $inputFieldMap) {
                if (array_key_exists('id', $inputFieldMap)) {
                    $fieldMap = FieldMap::find($inputFieldMap['id']);
                } else {
                    $fieldMap = new FieldMap();
                }

                $fieldMap->target = $inputFieldMap['target'];
                $fieldMap->output = $inputFieldMap['output'];
                $fieldMap->field_map_set_id = $fieldMapSet->id;
                $fieldMap->save();
            }
        }

        return $fieldMapSet->load('fieldMaps');
    }

    public function duplicate(Request $request, string $id)
    {
        $fieldMapSet = FieldMapSet::find($id);
        $newFieldMapSet = new FieldMapSet();
        $newFieldMapSet->name = $request->input('name', '');
        $newFieldMapSet->save();
        $newFieldMapSet->duplicateFieldMapsFromFieldMapSet($fieldMapSet);
        return $newFieldMapSet;
    }

    /**
     * Field Map Set delete
     *
     * @param  $id
     * @return void
     */
    public function delete($id)
    {
        $fieldMapSet = FieldMapSet::find($id);
        $fieldMapSet->delete();
    }
}
