<?php

namespace App\Http\Controllers;

use App\Campaign;
use Illuminate\Http\Request;
use App\EndPoint;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Campaign::with('endPoints')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campaign = new Campaign();

        $campaign->name = $request->input('name');
        $campaign->key = $request->input('key');
        $campaign->save();

        return $campaign;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function show(Campaign $campaign)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function edit(Campaign $campaign)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Campaign $campaign)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function destroy(Campaign $campaign)
    {
        //
    }

    public function syncEndPoints($id, Request $request)
    {
        $campaign = Campaign::find($id);

        $endPointIds = collect();

        foreach ($request->all() as $endPointData) {
            if (array_key_exists('id', $endPointData)) {
                $endPointIds->push($endPointData['id']);
            } else {
                $endPoint = new EndPoint();
                $endPoint->headers = (array_key_exists('headers', $endPointData)) ? $endPointDatat['headers'] : [];
                $endPoint->name = $endPointData['name'];
                $endPoint->token = $endPointData['token'];
                $endPoint->active = $endPointData['active'];
                $endPoint->field_map_set_id = (array_key_exists('field_map_set_id', $endPointData)) ? $endPointData['field_map_set_id'] : null;
                $endPoint->url = $endPointData['url'];
                $endPoint->filtered_fields = (array_key_exists('filtered_fields', $endPointData)) ? $endPointData['filtered_fields'] : [];
                $endPoint->field_map_template = $endPointData['field_map_template'];
                $endPoint->field_maps = (array_key_exists('field_maps', $endPointData)) ? $endPointData['field_maps'] : [];
                $endPoint->save();
                $endPointIds->push($endPoint->id);
            }
        }
        $campaign->endPoints()->sync($endPointIds->toArray());
        return $campaign;
    }

    public function syncEndPoint($id, Request $request)
    {
        $campaign = Campaign::find($id);

        if (! $campaign->endPoints->contains($request->input('id'))) {
            $campaign->endPoints()->attach([$request->input('id')]);
            $campaign->save();
            return $campaign->load('endPoints');
        } else {
            return response('End Point already assigned to this Campaign.', 400)
            ->header('Content-Type', 'text/plain');
        }
    }

    public function removeEndPointFromCampaign($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $campaign->endPoints()->detach($request->input('id'));
        $campaign->save();
        return $campaign;
    }
    
    public function deleteCampaign($id)
    {
        $campaign = Campaign::find($id);
        $campaign->delete();
        return response('Campaign deleted!', 200)
        ->header('Content-Type', 'text/plain');
    }
}
