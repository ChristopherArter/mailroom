<?php

namespace App\Http\Controllers;

use App\CampaignGroup;
use Illuminate\Http\Request;

class CampaignGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CampaignGroup::with('campaigns')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campaignGroup = new CampaignGroup();
        $campaignGroup->name = $request->input('name');
        $campaignGroup->save();
        $campaignGroup->attachCampaigns($request);
        return $campaignGroup;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CampaignGroup  $campaignGroup
     * @return \Illuminate\Http\Response
     */
    public function show(CampaignGroup $campaignGroup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CampaignGroup  $campaignGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(CampaignGroup $campaignGroup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CampaignGroup  $campaignGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CampaignGroup $campaignGroup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CampaignGroup  $campaignGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(CampaignGroup $campaignGroup)
    {
        //
    }

    public function sync(Request $request)
    {
        if ($request->input('id')) {
            $campaignGroup = CampaignGroup::find($request->input('id'));
        } else {
            $campaignGroup = new CampaignGroup();
        }

        $campaignGroup->name = $request->input('name', 'undefined');
        $campaignGroup->save();
        return $campaignGroup;
    }

    // public function sync(Request $request){
    //     if(!$request->input('id'))
    //     {
    //         $campaignGroup = new EndPointSet();
    //     } else {
    //         $campaignGroup = EndPointSet::find($request->input('id'));
    //     }
    //     $campaignGroup->name = $request->input('name', 'undefined');
    //     $campaignGroup->save();
    //     if(! empty($request->input('end_points')) ){
    //         $campaignGroup->attachEndPoints($request);
    //     }
    //     return $campaignGroup->load('endPoints');
    // }

    public function delete(Request $request)
    {
        if ($campaignGroup = CampaignGroup::find($request->input('id'))) {
            $campaignGroup->delete();
        }
        return response('campaign group deleted', 200)
            ->header('Content-Type', 'text/plain');
    }
}
