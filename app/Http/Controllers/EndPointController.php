<?php

namespace App\Http\Controllers;

use App\EndPoint;
use App\EndPointSet;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\FieldMap;

class EndPointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return EndPoint::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function sync(Request $request)
    {


        
        // it's an existing resource.
        if ($request->input('id')) {
            $endPoint = EndPoint::find($request->input('id'));
            $endPoint->updated_at = Carbon::now();
        } else {
            $endPoint = new EndPoint();
        }

        $endPoint->headers = $request->input('headers', []);
        $endPoint->name = $request->input('name');
        $endPoint->token = $request->input('token');
        $endPoint->active = $request->input('active');
        $endPoint->field_map_set_id = $request->input('field_map_set_id', null);

        $endPoint->url = $request->input('url');
        $endPoint->filtered_fields = $request->input('filtered_fields');
        $endPoint->field_map_template = (boolean) $request->input('field_map_template');
        $endPoint->field_maps = null;
        $endPoint->field_maps = $request->input('field_maps', null);
        $endPoint->save();
        return $endPoint;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EndPointSet $endPointSet, Request $request)
    {
        $endPoint = new EndPoint();
        $endPoint->url = $request->input('url', '');
        if ($request->input('headers')) {
            $endPoint->setHeaders($request->input('headers'));
        }

        $endPoint->save();
        $endPoint->endPointSet()->save($endPointSet);
        $endPoint->save();
        
        return $endPoint;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EndPoint  $endPoint
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return EndPoint::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EndPoint  $endPoint
     * @return \Illuminate\Http\Response
     */
    public function edit(EndPoint $endPoint)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EndPoint  $endPoint
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $endPoint = EndPoint::findOrFail((int) $request->input('id'));
        $endPoint->url = $request->input('url', '');
        if ($request->input('headers')) {
            $endPoint->setHeaders($request->input('headers'));
        }

        $endPoint->save();
        return $endPoint;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EndPoint  $endPoint
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $endPoint = EndPoint::find($id);
        $endPoint->delete();
        return response('deleted!', 200)->header('Content-Type', 'text/plain');
    }
}
