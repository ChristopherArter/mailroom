<?php

namespace App\Http\Controllers;

use App\FieldMap;
use Illuminate\Http\Request;

class FieldMapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FieldMap  $fieldMap
     * @return \Illuminate\Http\Response
     */
    public function show(FieldMap $fieldMap)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FieldMap  $fieldMap
     * @return \Illuminate\Http\Response
     */
    public function edit(FieldMap $fieldMap)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FieldMap  $fieldMap
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FieldMap $fieldMap)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FieldMap  $fieldMap
     * @return \Illuminate\Http\Response
     */
    public function destroy(FieldMap $fieldMap)
    {
        //
    }

    public function sync(Request $request)
    {
        if ($request->input('id')) {
            $fieldMap = FieldMap::find($request->input('id'));
        } else {
            $fieldMap = new FieldMap();
        }

        $fieldMap->active = $request->input('active');
        $fieldMap->end_point_id = $request->input('end_point_id', null);
        $fieldMap->field_set_id = $request->input('field_set_id', null);
        $fieldMap->output = $request->input('output', '');
        $fieldMap->target = $request->input('target', 'first_name');
        $fieldMap->save();
        return $fieldMap;
    }

    /**
     * Delete field map
     *
     * @param int $id
     * @return void
     */
    public function delete($id)
    {
        $fieldMap = FieldMap::find($id);
        $fieldMap->delete();
    }
}
