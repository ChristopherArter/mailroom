<?php

namespace App\Http\Controllers;

use App\RequestRecord;
use Illuminate\Http\Request;

class RequestRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RequestRecord  $requestRecord
     * @return \Illuminate\Http\Response
     */
    public function show(RequestRecord $requestRecord)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RequestRecord  $requestRecord
     * @return \Illuminate\Http\Response
     */
    public function edit(RequestRecord $requestRecord)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RequestRecord  $requestRecord
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequestRecord $requestRecord)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RequestRecord  $requestRecord
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequestRecord $requestRecord)
    {
        //
    }
}
