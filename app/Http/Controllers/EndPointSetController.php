<?php

namespace App\Http\Controllers;

use App\EndPointSet;
use App\Site;
use App\EndPoint;

use Illuminate\Http\Request;

class EndPointSetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return EndPointSet::with('endPoints')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function sync(Request $request)
    {
        if (!$request->input('id')) {
            $endPointSet = new EndPointSet();
        } else {
            $endPointSet = EndPointSet::find($request->input('id'));
        }
        $endPointSet->name = $request->input('name', 'undefined');
        $endPointSet->save();
        if (! empty($request->input('end_points'))) {
            $endPointSet->attachEndPoints($request);
        }
        return $endPointSet->load('endPoints');
    }

    public function delete(Request $request)
    {
        if ($endPointSet = EndPointSet::find($request->input('id'))) {
            $endPointSet->delete();
        }
        return response('end point set deleted', 200)
            ->header('Content-Type', 'text/plain');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $endPointSet = new EndPointSet();
        $endPointSet->name = $request->input('name');
        $endPointSet->save();
        $endPointSet->attachEndPoints($request);
        return $endPointSet;
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\EndPointSet  $endPointSet
     * @return \Illuminate\Http\Response
     */
    public function show(EndPointSet $endPointSet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EndPointSet  $endPointSet
     * @return \Illuminate\Http\Response
     */
    public function edit(EndPointSet $endPointSet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EndPointSet  $endPointSet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EndPointSet $endPointSet)
    {
        return $endPointSet;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EndPointSet  $endPointSet
     * @return \Illuminate\Http\Response
     */
    public function destroy(EndPointSet $endPointSet)
    {
        //
    }
}
