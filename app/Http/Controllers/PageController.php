<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RequestRecord;
use App\Site;

class PageController extends Controller
{
    public function monitor()
    {
        $requestRecords = RequestRecord::orderBy('id', 'desc')->take(1000)->get();
        return view('pages.monitor')->with('requestRecords', $requestRecords);
    }

    public function manageSites()
    {
        $returnSites = collect();
        $sites = Site::all();
        foreach ($sites as $site) {
            $site = $site->load('endPoints');
            $intakes->push($site);
        }
        return view('pages.manage_sites')->with('sites', $sites);
    }

    public function endPoints()
    {
        return view('pages.end_points');
    }

    public function manageEndPointSets()
    {
        return view('pages.campaign_groups');
    }

    public function manageCampaigns()
    {
        return view('pages.campaigns');
    }

    public function manageFieldMapTemplates()
    {
        return view('pages.field_map_templates');
    }
}
