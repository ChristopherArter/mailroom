<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class UserController extends Controller
{
    
    /**
     * return the user settings to the
     * front end.
     *
     * @param int $id
     * @return void
     */
    public function getSettings($id)
    {
        $user = Auth::user($id);
        return $user->settings;
    }

    public function getUser()
    {
        $user = Auth::user();

        if ($user && !$user->settings) {
            $user->settings = config('users.default_user_settings');
            $user->save();
        }
        return Auth::user();
    }

    public function getSettingByKey(string $key = null)
    {
        if ($user = Auth::user()) {
            if ($key && array_key_exists($key, $user->settings)) {
                return $user->settings[$key];
            } else {
                return $this->getSettings($user->id);
            }
        }
    }
}
