<?php

namespace App\Http\Controllers;

use App\Site;
use Illuminate\Http\Request;
use App\Endpoint;
use Illuminate\Support\Facades\Log;
use App\EndPointSet;
use App\Jobs\ProcessRequest;
use App\Providers\Sender;

class SiteController extends Controller
{

    /**
     * Route: GET /json/sites/{key?}
     *
     * Return site object for key passed to endpoint.
     * If no key is explicitly passed, it wil return
     * all.
     *
     * @param string $key
     * @return void
     */
    public function getSites(string $key = null)
    {
        if ($key) {
            $site = Site::where('api_key', $key)->get();

            if (! $site->isEmpty()) {
                return $site;
            } else {
                return abort(400, $key . ' site key does not exist.');
            }
        }
        return Site::all();
    }

    /**
     * return index
     *
     * @return void
     */
    public function jsonIndex()
    {
        return Site::all();
    }

    /**
     *  CRUD ROUTES
     */


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $intakes = Site::all();
        foreach ($intakes as $intake) {
            $intake->load('endPointSets');
            foreach ($intake->endPointSets as $endPointSet) {
                $endPointSet->load('endPoints');
                // foreach($endPointSet->endPoints as $endPoint)
                // {
                //     // if(!$endPoint->headers){
                //         $endPoint->headers = [ [ 'key' => 'foo', 'value' => 'bar'] ];
                //         $endPoint->save();
                //     // }

                // }
                $endPointSet->save();
            }
        }
        return view('intakes.intakes_index')->with('intakes', $intakes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $site = new Site();
        $site->fill($request->all());
        $site->save();
        return $site;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Site  $intake
     * @return \Illuminate\Http\Response
     */
    public function show(Site $site)
    {
        //return view('intakes.intakes_show')->with('intake', $intake);
        return $site;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Site  $intake
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, string $id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Site  $intake
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $site = Site::find($request->input('id'));
        $site->domain = $request->input('domain', '');
        $site->name = $request->input('name', 'Untitled Site');
        $site->api_key = $request->input('api_key', '');
        $site->email_distro = $request->input('email_distro', '');
        $site->slack_channel = $request->input('slack_channel', '');
        $site->active = $request->input('active');

        $site->save();
        return $site;
    }

    public function sync(Request $request)
    {
        if ($request->input('id')) {
            return $this->update($request);
        } else {
            return $this->store($request);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Site  $intake
     * @return \Illuminate\Http\Response
     */
    public function destroy(Site $intake)
    {
        //
    }

    /**
     * Oh fuck, turn all the sites on. or just one.
     *
     * @param Request $request
     * @param [type] $id
     * @return void
     */
    public function ohFuck(Request $request, $id = null)
    {
        if ($id) {
            $site = Site::find($id);
            $site->activate();
        } else {
            $sites = Site::all();

            foreach ($sites as $site) {
                $site->activate();
            }
        }
    }

    /**
     * Oh nevermind, turn all the sites off. or just one.
     *
     * @param Request $request
     * @param [type] $id
     * @return void
     */
    public function ohNevermind(Request $request, $id = null)
    {
        if ($id) {
            $site = Site::find($id);
            $site->deActivate();
        } else {
            $sites = Site::all();

            foreach ($sites as $site) {
                $site->deActivate();
            }
        }
    }
}
