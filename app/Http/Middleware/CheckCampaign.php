<?php

namespace App\Http\Middleware;

use Closure;
use App\Campaign;

class CheckCampaign
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /**
         *  Does the campaign exist?
         */

        $campaign = Campaign::where('name', $request->input('fields')['ruid'])->get();
        if ($campaign->isEmpty()) {
            $rejection = new Rejection();
            $rejection->reject($request, 'middleware: check campaign', 'Could not find a campaign with the name of ' . $request->input('fields')['ruid'], $request->input('request_id', ''));
            abort(400, 'Could not find a campaign with the name of ' . $request->input('fields')['ruid']);
        }
        
        return $next($request);
    }
}
