<?php

namespace App\Http\Middleware;

use Closure;
use App\Site;

class CheckSite
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $site = Site::find((int) $request->input('site_id'));
        /**
         *  Does the site exist?
         */
        if (! $site) {
            abort(400, 'Could not find a site with the ' . config('app.fields.site-key') . ' of ' . $request->input('site_id'));
        }
       
        return $next($request);
    }
}
