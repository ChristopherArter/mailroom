<?php

namespace App\Http\Middleware;

use Closure;

class CheckRequestOrigin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (config('app.restrict_ips')) {
            if (in_array($request->ip(), config('app.whitelist_ips'))) {
                return $next($request);
            } else {
                return response('Nope', 403)
                ->header('Content-Type', 'text/plain');
            }
        }

        return $next($request);
    }
}
