<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use App\ResponseRecord;
use App\Events\MalformedRequest;

class CheckForIntakeField
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->input(config('app.end_point_set_field'))) {
            return $next($request);
        } else {
            new MalformedRequest($request);
            return response('Bad Request', 400)
            ->header('Content-Type', 'text/plain');
        }
    }
}
