<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use App\Events\MalformedRequest;
use App\AllowedField;
use App\Rejection;
use App\Notifications\RejectionNotification;

class CheckFields
{
    /**
     * This checks to see if a request:
     * 1) Has standard fields
     * 2) Has a site id
     * 3) has a campaign name
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->isMethod('post')) {
            
            /**
             *  Check the database table allowed_fields
             */
            $acceptableFields = [];
            foreach (AllowedField::all() as $allowedField) {
                if ($allowedField->allowed) {
                    array_push($acceptableFields, $allowedField->name);
                }
            }

            foreach ($request->input('fields') as $key => $value) {
                if (! in_array($key, $acceptableFields)) {
                    $message = 'Use of non-standard field name ' . $key;
                    

                    $rejection = new Rejection();
                    $rejection->reject($request, 'middleware: check fields', $message, $request->input('request_id', ''));
                    // new MalformedRequest( $rejection, $request );

                    abort(400, 'Use of non-standard field name' . $key);
                }
            }

            if (! array_key_exists('ruid', $request->input('fields'))) {
                $rejection = new Rejection();
                $rejection->reject($request, 'middleware', 'Missing site key or campaign key', $request->input('request_id', ''));
                new MalformedRequest($rejection, $request);
                abort(400, 'Missing site key or campaign key');
            }
                

            return $next($request);
        }
    }
}
