<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllowedField extends Model
{
    protected $table = 'allowed_fields';
}
