<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Lead;
use Carbon\Carbon;

/**
 * This class is used to contain
 * the logic and management of the history
 * portion of a lead. This class is where
 * any logic related to inserting records
 * in the history of a lead (or any other model
 * eventually should live.
 */

class Tracker extends Model
{

    /**
     * Insert an event in the column of a lead model.
     *
     * @param string $event
     * @param App\Lead $lead
     * @param mixed $data
     * @param string $type
     * @return void
     */
    public static function record(string $event, Lead $lead = null, $data = null, string $type = null)
    {
        if ($lead && is_array($lead->history)) {
            $history = $lead->history;
        } else {
            $history = [];
        }

        array_push($history, [
            'event'         =>  $event,
            'data'          =>  $data,
            'type'          =>  $type,
            'micro'         =>  microtime(true),
            'created_at'    =>  Carbon::now()
        ]);

        return $history;
    }
}
