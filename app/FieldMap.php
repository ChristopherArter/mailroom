<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EndPoint;
use App\Lead;
use App\FieldMapSet;

class FieldMap extends Model
{
    protected $fillable = ['id'];
    public static function map(EndPoint $endPoint, Lead $lead)
    {
        if (! $endPoint->fieldMaps->isEmpty()) {
            $fields = $lead->fields;
            
            foreach (self::resolveFields($endPoint) as $fieldMap) {
                if (array_key_exists($fieldMap->target, $fields)) {
                    $value = $fields[$fieldMap->target];
                    unset($fields[$fieldMap->target]);
                    $fields[$fieldMap->output] = $value;
                }
            }
            return $fields;
        }
        return $lead->fields;
    }

    public static function resolveFields(EndPoint $endPoint)
    {
        if ($endPoint->fieldMapSet && $endPoint->fieldMapSet->fieldMaps) {
            return $endPoint->fieldMapSet->fieldMaps;
        } elseif (! $endPoint->fieldMaps->isEmpty()) {
            return $endPoint->fieldMaps;
        }
    }

    public function fieldMapSets()
    {
        return $this->belongsTo('App\FieldMapSet');
    }
    public function fieldMapSet()
    {
        return $this->belongsTo('App\FieldMapSet', 'field_map_set_id');
    }
}
