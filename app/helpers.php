<?php

/**
 * slugify strings
 *
 * @param string $strubg
 * @param string $delimiter
 * @return void
 */
function slugify(string $strubg, string $delimiter = '-')
{
    $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $strubg))))), $delimiter));
    return $slug;
}
