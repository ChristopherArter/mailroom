<?php
use App\Http\Middleware\CheckFields;
use App\Lead;
use App\EndPoint;
use App\AllowedField;
use App\Campaign;
use App\EndPointSet;
use App\FieldMap;
use App\FieldMapSet;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
   
Route::get('/home', 'HomeController@index')->name('home');

Route::post(
    'sites/inbound/{site}/{endPointSet?}',
 [ 'uses' => 'SiteController@handleInbound']
 )->name('sites.inbound');


Route::get('sites/{endPointName}', 'EndPointSetController@getEndPoints');

 /**
 *  PAGES
 */

    // Route::get('/monitor/leads', 'PageController@monitor')->name('pages.monitor');
    Route::get('/sites', 'PageController@manageSites')->name('pages.sites');
    Route::get('/field-map-templates', 'PageController@manageFieldMapTemplates')->name('pages.field-map-templates');

    Route::resource('endpointset', 'EndPointSetController')->only([
        'index', 'show', 'edit',
    ]);




    Route::post('endpoint/update/', 'EndPointController@update')->name('updateendpoint');
    Route::post('endpoint/{endPointSet}', 'EndPointController@store');


    Route::resource('leads', 'LeadController')->only([
        'store'
    ])->middleware('intake');

    Route::resource('leads', 'LeadController')->except([
        'create', 'store', 'update', 'destroy', 'index'
    ]);

    Route::get('/leads', function () {
        $leads = collect();
        foreach (Lead::take(10)->orderByDesc('created_at')->get() as $lead) {
            $lead->load('campaign');
            $lead->campaign->load('endPoints');
            $lead->load('responseRecords');
            $leads->push($lead);
        }
        return view('layouts.leads')->with('leads', $leads);
    });


    Route::get('/endpoints/sets', 'PageController@manageEndPointSets');
    Route::get('/campaigns', 'PageController@manageCampaigns');

/**
 *  RESOURCES
 */

Route::group(['prefix' => 'json'], function () {

    /**
     *  Lead resource
     */
    Route::get('/leads/', 'LeadController@index');
    Route::post('/leads', 'LeadController@store')->middleware('intake');
    Route::get('/leads/ids', 'LeadController@indexByIds')->name('leads.index.ids');
    Route::post('/leads/resend', 'LeadController@resendLeads');
    Route::get('/leads/site/{siteId?}', 'LeadController@showWithSite');
    Route::post('/leads/site/{siteId}/query', 'LeadController@query');
    Route::get('/leads/show/{id}', 'LeadController@show');
    Route::get('/leads/monitor/latest', 'LeadController@latest');
    Route::get('/leads/monitor/latest/id', 'LeadController@latestId');
    Route::get('/leads/{id}/history', 'LeadController@leadHistory');


    /**
     *  Endpoint resource
     */
    Route::resource('endpoints', 'EndPointController');
    Route::resource('endpointsets', 'EndPointSetController');
    Route::post('endpointsets/sync', 'EndPointSetController@sync');
    Route::post('endpointsets/delete', 'EndPointSetController@delete');

    /**
     * Response record refire
    */
    Route::post('responserecords/refire/{id}', 'ResponseRecordController@refire');
    Route::get('responserecords/{responserecord}', 'ResponseRecordController@show');

    /**
     *  Sites
    */
    Route::get('sites', 'SiteController@jsonIndex');
    Route::post('sites/sync', 'SiteController@sync');

    /**
     *  Campaigns
     */

    Route::get('campaigns', 'CampaignController@index');
    Route::post('campaigns/{id}/endpoints/sync', 'CampaignController@syncEndpoints');
    Route::post('endpoints/sync', 'EndPointController@sync');
    Route::post('endpoints/delete/{id}', 'EndPointController@destroy');
    Route::post('campaigns/{id}/endpoint/sync', 'CampaignController@syncEndpoint');
    Route::post('campaigns/{id}/endpoint/delete', 'CampaignController@removeEndPointFromCampaign');
    Route::post('campaigns/{id}/delete', 'CampaignController@deleteCampaign');
    Route::post('campaigns/store', 'CampaignController@store');


    /**
     *  Campaign Group Controller
    */
    Route::get('campaign-groups', 'CampaignGroupController@index');
    Route::post('campaign-groups/sync', 'CampaignGroupController@sync');
    Route::post('campaign-groups/delete', 'CampaignGroupController@delete');

    /**
     *  Field Maps
    */
    Route::post('fieldmaps/sync', 'FieldMapController@sync');
    Route::post('fieldmaps/delete/{id}', 'FieldMapController@delete');

    /**
     *  Field Map Templates
    */
    Route::get('fieldmaptemplates/', 'FieldMapSetController@index');
    Route::post('fieldmaptemplates/sync', 'FieldMapSetController@sync');
    Route::post('fieldmaptemplates/{id}/delete', 'FieldMapSetController@delete');

    Route::get('/allowed-fields', function () {
        $fields = collect();
        foreach (DB::table('allowed_fields')->select('name')->get() as $name) {
            $fields->push($name->name);
        }
        return $fields;
    });

    Route::get('sites/{key?}', 'SiteController@getSites');
});


/**
 *  USERS
 */
    Route::group(['prefix' => 'users'], function () {
        Route::get('/{id}/settings', 'UserController@getSettings');
        Route::get('current', 'UserController@getUser');
        Route::get('current/settings/{setting?}', 'UserController@getSettingByKey');
    });


Route::post('/filterfields/{endPoint}', function ($endPoint) {
    $lead = Lead::find(166);
    $endPoint = EndPoint::find($endPoint);
    return $endPoint->filterFields($lead);
});


Route::group(['prefix' => 'oh'], function () {
    Route::get('fuck/{id?}', 'SiteController@ohFuck');
    Route::get('nevermind/{id?}', 'SiteController@ohNevermind');
});

Route::get('endpoints', 'PageController@endPoints');



/**
 *  SETTINGS
 */
    Route::group(['prefix' => 'settings'], function () {
        Route::get('fields', function () {
            return config('fields.standard');
        });
    });
