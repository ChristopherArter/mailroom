<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LeadTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->json(
            '/json/leads',
        [
            'first_name'=> 'foo',
            'last_name' =>  'bar'
        ]
    );

        $response->assertStatus(201);
    }
}
