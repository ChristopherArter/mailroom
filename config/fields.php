<?php

return [

/**
 *  STANDARD FIELDS
 *
 * This list of fields
 * is what the middleware
 * checks requests
 * against when they hit
 * the intake route.
 *
 * You best come correct.
 *
 * This will be replaced by a
 * database table later on.
 */

    'standard' => [
        'full_name',
        'first_name',
        'last_name',
        'phone' ,
        'best_time_to_call',
        'email',
        'address',
        'zip_code',
        'country',
        'comments',
        'requested_service',
        'cause',
        'effect',
        'military_branch',
        'vaccine_year',
        'vaccine_type',
        'tcpa_accepted',
        'tcpa_agreement',
        'payment_start_date',
        'type_of_payment',
        'payment_amount',
        'number_of_payments',
        'payment_period',
        'quote_amount',
        'company',
        'job_title',
        'fault',
        'serious_injury',
        'lead_type',
        'ip_address',
        'heirial2',
        'heirial2_data',
        'heirial2_uid',
        'heirial2_vid',
        'heirial2_lid',
        'telescope_id',
        'pardot_visitor_id',
        'ga_user_id',
        'click_path',
        'user_agent',
        'device',
        'browser',
        'operating_system',
        'cta_id',
        'ga_form_name',
        'ga_form_location',
        'is_spanish',
        'vertical',
        'source',
        'organization_id',
        'affiliate_id',
        'law_firm',
        'referrer',
        'is_paid',
        'lead_attribution',
        'mkwid',
        'gclid',
        'utm_source',
        'utm_campaign',
        'utm_medium',
        'utm_content',
        'utm_term',
        'adwords_matchtype',
        'adwords_network',
        'adwords_device',
        'adwords_devicemodel',
        'adwords_creative',
        'adwords_adposition',
        'debug',
        'debug_email',
        config('app.end_point_set_field')
    ]

];
