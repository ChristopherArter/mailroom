<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldMapFieldMapSetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field_map_field_map_set', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('field_map_id');
            $table->integer('field_map_set_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field_map_field_map_set');
    }
}
