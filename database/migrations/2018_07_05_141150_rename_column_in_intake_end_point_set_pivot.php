<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnInIntakeEndPointSetPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('end_point_set_site', function (Blueprint $table) {
            $table->renameColumn('end_point_id', 'end_point_set_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('end_point_set_site', function (Blueprint $table) {
            $table->renameColumn('end_point_set_id', 'end_point_id');
        });
    }
}
