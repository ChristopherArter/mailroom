<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldMapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field_maps', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('field_set_id')->nullable();
            $table->integer('end_point_id');
            $table->string('target');
            $table->string('output');
            $table->boolean('active')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field_maps');
    }
}
